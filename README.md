# Datavisualization @ Onderzoek en Statistiek

This repository contains the code for

- a [website](https://onderzoek.amsterdam.nl/interactief/datavisualisatie) with examples of (interactive) data visualizations made by the Research and Statistics department (Onderzoek en Statistiek) of the Municipality of Amsterdam. The site also contains instructions and tools to create data visualizations with different programming languages, like R, Python and JavaScript.
- the [web component](./webcomponent/README.md) `<os-visualisation>` that is used to render both Vega-Lite visualisations and interactive maps.
- assets like geometries and color specs that can be used in other projects.
