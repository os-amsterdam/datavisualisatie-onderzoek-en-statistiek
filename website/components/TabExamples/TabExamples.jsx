import Card from '../Card/Card'

import cardConfig from './index.json'
import styles from './TabExamples.module.css'

function Examples({ currentTab }) {
  return currentTab === 'voorbeelden' ? (
    <>
      <p className="mb-20">
        Hieronder vind je voorbeelden van door O&S veelgebruikte visualisaties.
        Klik op de grafiek of de kaart om deze in de online editor te bewerken,
        bijvoorbeeld door eigen data toe te voegen. Snel data uit een
        spreadsheet omzetten naar json kan met&nbsp;
        <a
          data-variant="inline"
          href="https://shancarter.github.io/mr-data-converter/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          Mr Data Converter
        </a>
        .
      </p>
      {cardConfig.map(({ type, name, specs }) => (
        <div key={type}>
          <h2 data-style-as="h3" className="mb-20">
            {name}
          </h2>
          <div className={styles.container}>
            {specs.map(({ id, title }) => (
              <Card key={id} title={title} spec={`specs/vega/${id}.vg.json`} />
            ))}
          </div>
        </div>
      ))}
    </>
  ) : (
    <os-icon name="spinner" size="2rem" color="var(--blue)" />
  )
}
export default Examples
