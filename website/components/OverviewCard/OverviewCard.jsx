import { openEditor } from '../../lib/utils'

import styles from './OverviewCard.module.css'

function OverviewCard({ spec, url }) {
  return (
    <div className={styles.card}>
      <div className={styles.vis}>
        <os-visualisation
          spec={JSON.stringify(spec)}
          no-footer
          no-border
        ></os-visualisation>
      </div>
      <footer>
        <button type="button" onClick={() => openEditor(spec)} data-small>
          <os-icon name="external-link" />
          naar editor
        </button>
        <button
          type="button"
          onClick={() => window.open(url, '_blank')}
          data-small
        >
          <os-icon name="external-link" />
          naar pagina
        </button>
      </footer>
    </div>
  )
}

export default OverviewCard
