import { useEffect, useState } from 'react'
import ColorContrastChecker from 'color-contrast-checker'

import CodeBlock from '../CodeBlock/CodeBlock'

import styles from './TabColorpicker.module.css'
import {
  generateSequentialScheme,
  generateDivergentScheme,
  hexToRgb,
  getMaxColors,
} from './utils'

const DEFAULT_AMOUNT = 5
const OTHER_COLOR = '#e6e6e6'

const ccc = new ColorContrastChecker()

function Colorpicker() {
  const [type, setType] = useState('oplopend')
  const [amount, setAmount] = useState(DEFAULT_AMOUNT)
  const [hasOther, setHasOther] = useState(false)
  const [scheme, setScheme] = useState('blauw (1-9)')
  const [allColors, setAllColors] = useState()
  const [palette, setPalette] = useState()
  const [reversePalette, setReversePalette] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()

    fetch('colors/colors.json', {
      signal: abortController.signal,
      mode: 'cors',
    })
      .then((response) => response.json())
      .then((json) => {
        setAllColors(json)
      })
    return () => abortController.abort()
  }, [])

  useEffect(() => {
    if (allColors) {
      let colors
      const selection = allColors[type][scheme]

      if (type === 'oplopend') {
        colors = generateSequentialScheme(
          selection[0],
          selection[1],
          parseInt(amount, 10),
        ).slice(0, -1)
      }
      if (type === 'uiteenlopend') {
        colors = generateDivergentScheme(
          selection[0],
          selection[1],
          selection[2],
          parseInt(amount, 10),
        )
      }
      if (type === 'discreet') {
        const maxAmount =
          getMaxColors(scheme) < amount ? getMaxColors(scheme) : amount
        colors = allColors[type][scheme][maxAmount]
      }

      const orderedColors = reversePalette ? colors.slice().reverse() : colors

      if (hasOther) {
        setPalette([...orderedColors, OTHER_COLOR])
      } else {
        setPalette(orderedColors)
      }
    }
  }, [allColors, type, amount, scheme, hasOther, reversePalette])

  const handleTypeChange = (e) => {
    if (e.target.value === 'oplopend') {
      setScheme('blauw (1-9)')
      setAmount(DEFAULT_AMOUNT)
    }
    if (e.target.value === 'uiteenlopend') {
      setScheme('stoplicht (1-7)')
      setAmount(DEFAULT_AMOUNT)
    }
    if (e.target.value === 'discreet') {
      setScheme('discreet (1-9)')
      setAmount(DEFAULT_AMOUNT)
    }
    setType(e.target.value)
  }

  useEffect(() => {
    if (
      (scheme === 'oranje (1-5)' || scheme === 'lichtgroen (1-5)') &&
      amount > DEFAULT_AMOUNT
    ) {
      setAmount(DEFAULT_AMOUNT)
    }
    if (scheme !== 'duotone (2, 4, 6, 8, 10)' && amount > 9) {
      setAmount(9)
    }
  }, [scheme, amount])

  useEffect(() => {
    if (
      scheme === 'grijs (1-9, alleen icm kleur)' ||
      scheme === 'blauw - grijs - groen (1-9)' ||
      scheme === 'paars - grijs - lichtblauw (1-9)'
    ) {
      setHasOther(false)
    }
    if (scheme === 'stoplicht (1-7)') {
      setAmount(DEFAULT_AMOUNT)
    }
  }, [scheme])

  const textColors = palette?.map((color) =>
    ccc.isLevelAA(color, '#ffffff', 16) ? '#ffffff' : '#000000',
  )

  return (
    <>
      <div className="mb-40">
        <p className="mb-24">
          Genereer kleuren voor je visualisatie in de huisstijl.
        </p>
        <h2 data-style-as="h5" className="mb-24">
          Wat voor kleurenschema wil je hebben?
        </h2>
        <select value={type} onChange={handleTypeChange}>
          <option value="oplopend">op-/aflopend</option>
          <option value="uiteenlopend">uiteenlopend</option>
          <option value="discreet">discreet</option>
        </select>
      </div>
      <div className="mb-40">
        <h2 data-style-as="h5" className="mb-24">
          Selecteer kleurenschema
        </h2>
        {allColors && (
          <select value={scheme} onChange={(e) => setScheme(e.target.value)}>
            {Object.keys(allColors[type]).map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
          </select>
        )}
      </div>
      <div className="mb-40">
        <h2 data-style-as="h5" className="mb-24">
          Hoeveel kleuren wil je hebben?
        </h2>
        <div className={styles.inputContainer}>
          <input
            className="mb-12"
            type="number"
            min={1}
            max={getMaxColors(scheme)}
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
          />

          <label>
            <input
              type="checkbox"
              id="overig"
              onChange={() => setHasOther(!hasOther)}
              checked={hasOther}
              disabled={
                scheme === 'grijs (1-9, alleen icm kleur)' ||
                scheme === 'blauw - grijs - groen (1-9)' ||
                scheme === 'paars - grijs - lichtblauw (1-9)'
              }
            />
            Voeg ‘overig’ categorie toe
          </label>
        </div>
      </div>
      <div className="mb-40">
        <h2 data-style-as="h5" className="mb-24">
          Kopieer de kleuren in het formaat dat je wil hebben
        </h2>
        {palette && textColors && (
          <>
            <div className="mb-8">
              {palette.map((item) => (
                <div
                  className={styles.colorBlock}
                  key={item}
                  style={{
                    '--bgColor': item,
                    '--textColor': ccc.isLevelAA(item, '#fff', 16)
                      ? '#fff'
                      : '#000',
                  }}
                >
                  Aa
                </div>
              ))}
            </div>

            <label>
              <input
                type="checkbox"
                id="reverse"
                onChange={() => setReversePalette(!reversePalette)}
                checked={reversePalette}
              />
              Draai volgorde om
            </label>
            <div className={styles.codeBlockContainer}>
              <CodeBlock textColors={textColors.join(', ')}>
                {palette.join(', ')}
              </CodeBlock>
              <CodeBlock
                textColors={textColors.map((item) => `"${item}"`).join(', ')}
              >
                {palette.map((item) => `"${item}"`).join(', ')}
              </CodeBlock>
              <CodeBlock
                textColors={`[${textColors
                  .map((item) => `"${item}"`)
                  .join(', ')}]`}
              >
                {`[${palette.map((item) => `"${item}"`).join(', ')}]`}
              </CodeBlock>
              <CodeBlock
                textColors={`[${textColors.map(
                  (item) => `[${hexToRgb(item)}]`,
                )}]`}
              >
                {`[${palette.map((item) => `[${hexToRgb(item)}]`)}]`}
              </CodeBlock>
              <CodeBlock
                textColors={`c(${textColors
                  .map((item) => `"${item}"`)
                  .join(', ')})`}
              >
                {`c(${palette.map((item) => `"${item}"`).join(', ')})`}
              </CodeBlock>
            </div>
          </>
        )}
      </div>
      <div className="mb-40">
        <h2 data-style-as="h4" className="mb-24">
          Goed om te weten
        </h2>
        <h3 data-style-as="h5" className="mb-16">
          Tekstkleuren
        </h3>
        <p data-small className="mb-16">
          Als je tekst op gekleurde vakken gebruikt, dan moet deze tekst goed te
          onderscheiden zijn. Deze tekstkleuren zijn ook beschikbaar voor de
          schema’s hierboven, en kun je kopiëren met de knop ‘Kopieer
          tekstkleuren’.
        </p>
        <h3 data-style-as="h5" className="mb-16">
          Contrast
        </h3>
        <p data-small className="mb-16">
          Maak je een lijngrafiek? Probeer dan de lichtere kleuren te vermijden.
          Zo blijft elke lijn goed zichtbaar.
        </p>
        <h3 data-style-as="h5" className="mb-16">
          Kleurenblindheid
        </h3>
        <p data-small className="mb-16">
          De kleuren binnen de schema’s hierboven zijn goed te onderscheiden
          voor kleurenblinden. Kies je zelf verschillende kleuren of gebruik je
          binnen je rapport bepaalde kleuren vaak, let dan op! Sommige
          Amsterdamse huisstijlkleuren kun je beter niet combineren. De volgende
          combinaties zijn voor kleurenblinden niet goed te onderscheiden:
        </p>
        <div className={styles.colorComboContainer}>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#A00078',
              '--textColor': ccc.isLevelAA('#A00078', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #A00078
          </div>
          <span>&nbsp;+&nbsp;</span>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#004699',
              '--textColor': ccc.isLevelAA('#004699', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #004699{' '}
          </div>
          <span>&nbsp;=&nbsp;</span>
          <os-icon name="close" size="40px" color="var(--red)" />
        </div>
        <div className={styles.colorComboContainer}>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#009DE6',
              '--textColor': ccc.isLevelAA('#009DE6', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #009DE6
          </div>
          <span>&nbsp;+&nbsp;</span>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#00A03C',
              '--textColor': ccc.isLevelAA('#00A03C', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #00A03C{' '}
          </div>
          <span>&nbsp;=&nbsp;</span>
          <os-icon name="close" size="40px" color="var(--red)" />
        </div>
        <div className={styles.colorComboContainer}>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#EC0000',
              '--textColor': ccc.isLevelAA('#EC0000', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #EC0000
          </div>
          <span>&nbsp;+&nbsp;</span>
          <div
            className={styles.colorBlock}
            style={{
              '--bgColor': '#00A03C',
              '--textColor': ccc.isLevelAA('#00A03C', '#fff', 16)
                ? '#fff'
                : '#000',
            }}
          >
            #00A03C{' '}
          </div>
          <span>&nbsp;=&nbsp;</span>
          <os-icon name="close" size="40px" color="var(--red)" />
        </div>
        <p data-small>
          Wil je je eigen kleurenpalet testen? Check de kleuren die je gebruikt
          dan bijvoorbeeld op{' '}
          <a
            data-variant="inline"
            href="https://gka.github.io/palettes/"
            target="_blank"
            rel="noreferrer"
          >
            https://gka.github.io/palettes/
          </a>
        </p>
      </div>
    </>
  )
}

export default Colorpicker
