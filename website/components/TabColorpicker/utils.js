import chroma from 'chroma-js'

export const hexToRgb = (hex) =>
  hex
    .replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => `#${r}${r}${g}${g}${b}${b}`,
    )
    .substring(1)
    .match(/.{2}/g)
    .map((x) => parseInt(x, 16))

export const generateSequentialScheme = (
  startColor,
  endColor,
  numberOfColors,
) =>
  chroma
    .scale([startColor, endColor])
    .mode('lch')
    .colors(numberOfColors + 1)

export const generateDivergentScheme = (
  startColor,
  middleColor,
  endColor,
  numberOfColors,
) =>
  chroma
    .scale([startColor, middleColor, endColor])
    .mode('lch')
    .colors(numberOfColors)

export const getMaxColors = (scheme) => {
  switch (scheme) {
    case 'stoplicht (1-7)':
      return 7
    case 'oranje (1-5)':
      return 5
    case 'lichtgroen (1-5)':
      return 5
    case 'duotone (2, 4, 6, 8, 10)':
      return 10
    default:
      return 9
  }
}
