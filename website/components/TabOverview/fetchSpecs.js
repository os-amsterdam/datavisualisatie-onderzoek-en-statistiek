import qs from 'qs'

const query = {
  fields: ['slug', 'publishedAt', 'updatedAt'],
  filters: {
    publishedAt: {
      $gt: 2022,
    },
  },
  populate: {
    body: {
      on: {
        'shared.visualisation': { populate: '*' },
        'shared.panel-group': {
          populate: {
            panel: { populate: '*' },
          },
        },
      },
    },
  },
}

const CONTENT_TYPES = {
  articles: {
    type: 'article',
    name: 'artikel',
    plural: 'Artikelen',
  },
  publications: {
    type: 'publication',
    name: 'publicatie',
    plural: 'Publicaties',
  },
  videos: {
    type: 'video',
    name: 'video',
    plural: 'Videos',
  },
  collections: {
    type: 'collection',
    name: 'dossier',
    plural: 'Dossiers',
  },
}

const fetchSpecs = (url, type, signal) =>
  fetch(`${url}?fields=id`, { signal })
    .then((response) => response.json())
    .then((metaResult) =>
      fetch(
        `${url}?pagination[pageSize]=${
          metaResult.meta.pagination.total
        }&${qs.stringify(query, {
          encodeValuesOnly: true,
        })}`,
        { signal },
      )
        .then((response) => response.json())
        .then((json) => {
          const itemsWithPubDate = json.data.flatMap(
            ({ body, updatedAt, publishedAt, slug }) =>
              body.map((item) => ({
                ...item,
                updatedAt,
                publishedAt,
                slug,
              })),
          )

          const itemsWithSpecs = itemsWithPubDate.filter(
            ({ specification, panel }) =>
              specification ||
              panel?.filter((panelItem) => panelItem.specification),
          )

          const specs = itemsWithSpecs.flatMap(
            ({ id, specification, panel, updatedAt, publishedAt, slug }) =>
              specification
                ? {
                    id: `visualisation-${id}`,
                    updatedAt,
                    publishedAt,
                    spec: specification,
                    url: `https://onderzoek.amsterdam.nl/${CONTENT_TYPES[type].name}/${slug}`,
                  }
                : panel?.flatMap((panelItem) =>
                    panelItem.specification
                      ? {
                          id: `panel-${panelItem.id}`,
                          updatedAt,
                          publishedAt,
                          spec: panelItem.specification,
                          url: `https://onderzoek.amsterdam.nl/${CONTENT_TYPES[type].name}/${slug}`,
                        }
                      : [],
                  ),
          )

          return specs
        }),
    )

export default fetchSpecs
