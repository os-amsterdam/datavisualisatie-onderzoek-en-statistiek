import { useState, useEffect, useCallback } from 'react'

import OverviewCard from '../OverviewCard/OverviewCard'

import fetchSpecs from './fetchSpecs'
import styles from './TabOverview.module.css'

const endpointList = ['collections', 'articles', 'publications', 'videos']

function Overview({ currentTab }) {
  const [data, setData] = useState()
  const [page, setPage] = useState(1)

  // change page
  const updateCurrentPage = (record) => {
    const mutation = record[0]
    const newValue = mutation.target.attributes.getNamedItem(
      mutation.attributeName,
    ).value
    setPage(+newValue)
  }

  // listen to changes is pagination components
  const paginationRef = useCallback((node) => {
    if (node !== null) {
      const observer = new MutationObserver(updateCurrentPage)
      observer.observe(node, {
        attributes: true,
        attributeFilter: ['current'],
      })
    }
  }, [])

  // fetch all specs
  useEffect(() => {
    const controller = new AbortController()

    Promise.all(
      endpointList.map((endpoint) =>
        fetchSpecs(
          `https://cms.onderzoek-en-statistiek.nl/api/${endpoint}`,
          endpoint,
          controller.signal,
        ),
      ),
    ).then((arrays) => {
      setData(
        arrays
          .flat()
          //.filter(({ spec }) => spec.$schema.includes('vega'))
          //.sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt)),
          .sort((a, b) => new Date(b.publishedAt) - new Date(a.publishedAt)),
      )
    })

    return () => {
      controller.abort()
    }
  }, [])

  const totalPages = Math.ceil(data?.length / 9)

  return currentTab === 'overzicht' ? (
    <>
      <p className="mb-40">
        Een overzicht van alle visualisaties die op dit moment online staan op
        de website van O&S. Bekijk ze in de context van de pagina waarvoor ze
        zijn gemaakt of open ze in de online editor als basis voor je eigen
        visualisatie.
      </p>

      <div className={styles.cardContainer}>
        {data?.slice((page - 1) * 9, page * 9).map(({ id, spec, url }) => (
          <OverviewCard key={id} spec={spec} url={url} />
        ))}
      </div>

      <os-pagination
        ref={paginationRef}
        total={totalPages}
        url-params
      ></os-pagination>
    </>
  ) : (
    <os-icon name="spinner" size="2rem" color="var(--blue)" />
  )
}

export default Overview
