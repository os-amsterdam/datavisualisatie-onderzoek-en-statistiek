import { lazy, useState, useEffect, useRef } from 'react'

import TabExamples from '../TabExamples/TabExamples'
import TabOverview from '../TabOverview/TabOverview'

import styles from './App.module.css'

const TabColorpicker = lazy(() => import('../TabColorpicker/TabColorpicker'))
const TabAreas = lazy(() => import('../TabAreas/TabAreas'))
const TabEditor = lazy(() => import('../TabEditor/TabEditor'))

function App() {
  const tabRef = useRef()

  const [currentTab, setCurrentTab] = useState()

  const updateCurrentTab = () => {
    setCurrentTab(tabRef.current.getAttribute('active-tab'))
  }

  useEffect(() => {
    updateCurrentTab()
    const observer = new MutationObserver(updateCurrentTab)
    observer.observe(tabRef.current, {
      attributes: true,
    })
    return () => {
      observer.disconnect()
    }
  }, [tabRef])

  return (
    <div className={styles.container}>
      <h1 className="mb-40">Datavisualisatie O&amp;S</h1>
      <p className="mb-80" data-large>
        Hieronder vind je alles over datavisualisatie bij Onderzoek en
        Statistiek van de gemeente Amsterdam. We maken visualisaties met&nbsp;
        <a
          data-variant="inline"
          href="https://vega.github.io/vega-lite/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          Vega-lite
        </a>
        &nbsp;en gebruiken daarvoor de&nbsp;
        <a
          data-variant="inline"
          href="https://vega.github.io/editor/#/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          online editor
        </a>
        ,&nbsp;
        <a data-variant="inline" href="voorbeelden/visualisations-python.html">
          Python
        </a>
        &nbsp;of&nbsp;
        <a data-variant="inline" href="voorbeelden/visualisations-r.html">
          R
        </a>
        .
      </p>

      <os-tabs url-params ref={tabRef}>
        <ol
          role="tablist"
          aria-label="Kies uit een van de volgende tabs"
          className="mb-40"
        >
          <li>
            <a href="#voorbeelden" role="tab">
              Voorbeelden
            </a>
          </li>
          <li>
            <a href="#overzicht" role="tab">
              Overzicht
            </a>
          </li>
          <li>
            <a href="#kleuren" role="tab">
              Kleurenkiezer
            </a>
          </li>
          <li>
            <a href="#gebieden" role="tab">
              Gebieden
            </a>
          </li>
          <li>
            <a href="#editor" role="tab">
              Editor
            </a>
          </li>
        </ol>

        <div id="voorbeelden" role="tabpanel" className={styles.tabContent}>
          <TabExamples currentTab={currentTab} />
        </div>

        <div id="overzicht" role="tabpanel" className={styles.tabContent}>
          <TabOverview currentTab={currentTab} />
        </div>

        <div id="kleuren" role="tabpanel" className={styles.tabContent}>
          <TabColorpicker />
        </div>

        <div id="gebieden" role="tabpanel" className={styles.tabContent}>
          <TabAreas />
        </div>

        <div id="editor" role="tabpanel" className={styles.tabContent}>
          <TabEditor />
        </div>
      </os-tabs>
    </div>
  )
}

export default App
