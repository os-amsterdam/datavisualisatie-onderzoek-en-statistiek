import { openEditor } from '../../lib/utils'

import styles from './Card.module.css'

function Card({ title, spec }) {
  return (
    <button
      className={styles.card}
      type="button"
      onClick={() => openEditor(spec)}
    >
      <div className={styles.vis}>
        {spec && (
          <os-visualisation spec={spec} no-footer no-border></os-visualisation>
        )}
      </div>

      <h2 data-style-as="h5">{title}</h2>
    </button>
  )
}

export default Card
