import { useState, useEffect } from 'react'

import styles from './CodeBlock.module.css'

function CodeBlock({ children, textColors }) {
  const [hasCopiedColors, setHasCopiedColors] = useState(false)
  const [hasCopiedTextColors, setHasCopiedTextColors] = useState(false)

  useEffect(() => {
    if (hasCopiedColors) {
      setTimeout(() => {
        setHasCopiedColors(false)
      }, 2500)
    }
  }, [hasCopiedColors])

  useEffect(() => {
    if (hasCopiedTextColors) {
      setTimeout(() => {
        setHasCopiedTextColors(false)
      }, 2500)
    }
  }, [hasCopiedTextColors])

  return (
    <>
      <code className={styles.code}>{children}</code>
      <button
        type="button"
        data-small
        onClick={() => {
          navigator.clipboard.writeText(children)
          setHasCopiedColors(true)
        }}
      >
        <os-icon
          size="20px"
          name={hasCopiedColors ? 'checkmark' : 'document'}
        />
        Kopieer kleuren
      </button>
      <button
        type="button"
        data-small
        onClick={() => {
          navigator.clipboard.writeText(textColors)
          setHasCopiedTextColors(true)
        }}
      >
        <os-icon
          size="20px"
          name={hasCopiedTextColors ? 'checkmark' : 'document'}
        />
        Kopieer tekstkleuren
      </button>
    </>
  )
}

export default CodeBlock
