const years = [
  2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022,
]
const yearsPlus2023 = [
  2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022,
  2023,
]

const areasAmsterdam = [
  // Buurten Amsterdam

  {
    file: 'amsterdam/historisch/buurten-1850-1896-topo.json',
    description: 'Buurtindeling van 1850 tot en met 1896',
    geography: 'Buurten Amsterdam',
    period: '1850-1896',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurten-1896-1909-topo.json',
    description: 'Buurtindeling van 1896 tot en met 1909',
    geography: 'Buurten Amsterdam',
    period: '1896-1909',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurten-1909-1921-topo.json',
    description: 'Buurtindeling van 1909 tot en met 1921',
    geography: 'Buurten Amsterdam',
    period: '1909-1921',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurten-1921-1931-topo.json',
    description: 'Buurtindeling van 1921 tot en met 1931',
    geography: 'Buurten Amsterdam',
    period: '1921-1931',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurten-1986-1998-topo.json',
    description: 'Buurtindeling van 1986 tot en met 1998',
    geography: 'Buurten Amsterdam',
    period: '1986-1998',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code', 'buurtcombinatieNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelCode',
  },

  {
    file: 'amsterdam/historisch/buurten-1998-2005-topo.json',
    description: 'Buurtindeling van 1998 tot en met 2005',
    geography: 'Buurten Amsterdam',
    period: '1998-2005',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code', 'buurtcombinatieCode', 'stadsdeelCode'],
    colorField: 'stadsdeelCode',
  },

  {
    file: 'amsterdam/historisch/buurten-2005-2015-topo.json',
    description: 'Buurtindeling van 2005 tot en met 2015',
    geography: 'Buurten Amsterdam',
    period: '2005-2015',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code', 'buurtcombinatieNaam', 'stadsdeelCode'],
    colorField: 'stadsdeelCode',
  },

  {
    file: 'amsterdam/2015-2020/buurten-2015-2020-topo.json',
    description: 'Buurtindeling van 2015 tot en met 2020',
    geography: 'Buurten Amsterdam',
    period: '2015-2020',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2021/buurten-2021-topo.json',
    description: 'Tijdelijke buurtindeling inclusief Weesp in 2021',
    geography: 'Buurten Amsterdam',
    period: '2021',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/buurten-2022-topo.json',
    description: 'Buurtindeling sinds de herindeling van 24 maart 2022',
    geography: 'Buurten Amsterdam',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2015-2020/buurten-2015-2020-zw-topo.json',
    description:
      'Buurtindeling van 2015 tot en met 2020 exlusief waterpartijen',
    geography: 'Buurten Amsterdam',
    period: '2015-2020',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2021/buurten-2021-zw-topo.json',
    description:
      'Tijdelijke buurtindeling inclusief Weesp exlusief waterpartijen in 2021',
    geography: 'Buurten Amsterdam',
    period: '2021',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/buurten-2022-zw-topo.json',
    description:
      'Buurtindeling sinds de herindeling van 24 maart 2022 exclusief waterpartijen',
    geography: 'Buurten Amsterdam',
    period: '2022',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/buurten-2022-bewoond-topo.json',
    description:
      'Buurtindeling sinds de herindeling van 24 maart 2022 alleen bewoonde gebieden',
    geography: 'Buurten Amsterdam',
    period: '2022',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code', 'wijkNaam', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },

  // Buurtcombinaties Amsterdam

  {
    file: 'amsterdam/historisch/buurtcombinaties-1966-1986-topo.json',
    description: 'Indeling buurtcombinaties van 1966 tot en met 1986',
    geography: 'Buurtcombinaties Amsterdam',
    period: '1966-1986',
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurtcombinaties-1986-1998-topo.json',
    description: 'Indeling buurtcombinaties van 1986 tot en met 1998',
    geography: 'Buurtcombinaties Amsterdam',
    period: '1986-1998',
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurtcombinaties-1998-2005-topo.json',
    description: 'Indeling buurtcombinaties van 1998 tot en met 2005',
    geography: 'Buurtcombinaties Amsterdam',
    period: '1998-2005',
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code', 'stadsdeelCode'],
    colorField: 'code',
  },

  {
    file: 'amsterdam/historisch/buurtcombinaties-2005-2015-topo.json',
    description: 'Indeling buurtcombinaties van 2005 tot en met 2015',
    geography: 'Buurtcombinaties Amsterdam',
    period: '2005-2015',
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code', 'stadsdeelCode'],
    colorField: 'code',
  },

  // Wijken Amsterdam

  {
    file: 'amsterdam/2015-2020/wijken-2015-2020-topo.json',
    description: 'Wijkindeling van 2015 tot en met 2020',
    geography: 'Wijken Amsterdam',
    period: '2015-2020',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2021/wijken-2021-topo.json',
    description: 'Tijdelijke wijkindeling inclusief Weesp in 2021',
    geography: 'Wijken Amsterdam',
    period: '2021',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/wijken-2022-topo.json',
    description: 'Wijkindeling sinds de herindeling van 24 maart 2022',
    geography: 'Wijken Amsterdam',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2015-2020/wijken-2015-2020-zw-topo.json',
    description:
      'Wijkindeling van 2015 tot en met 2020 exclusief waterpartijen',
    geography: 'Wijken Amsterdam',
    period: '2015-2020',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2021/wijken-2021-zw-topo.json',
    description:
      'Tijdelijke wijkindeling inclusief Weesp, 2021 exclusief waterpartijen',
    geography: 'Wijken Amsterdam',
    period: '2021',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/wijken-2022-zw-topo.json',
    description:
      'Wijkindeling sinds de herindeling van 24 maart 2022 exclusief waterpartijen',
    geography: 'Wijken Amsterdam',
    period: '2022',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/wijken-2022-bewoond-topo.json',
    description:
      'Wijkindeling sinds de herindeling van 24 maart 2022, alleen bewoonde gebieden',
    geography: 'Wijken Amsterdam',
    period: '2022',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code', 'gebiedNaam', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2015-2020/gebieden-2015-2020-topo.json',
    description:
      'Indeling Gebiedsgericht Werken Gebieden van 2015 tot en met 2020',
    geography: 'Gebieden Amsterdam',
    period: '2015-2020',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'stadsdeelCode'],
    colorField: 'stadsdeelCode',
  },
  {
    file: 'amsterdam/2015-2020/gebieden-2015-2020-bewoond-topo.json',
    description:
      'Indeling Gebiedsgericht Werken Gebieden van 2015 tot en met 2020, bewoonde gebieden',
    geography: 'Gebieden Amsterdam',
    period: '2015-2020',
    type: { id: 'bewoond', label: 'bewoond' },
    tooltipFields: ['naam', 'code', 'stadsdeelCode'],
    colorField: 'stadsdeelCode',
  },
  {
    file: 'amsterdam/2021/gebieden-2021-topo.json',
    description:
      'Tijdelijke Gebiedsgericht Werken Gebieden inclusief Weesp in 2021',
    geography: 'Gebieden Amsterdam',
    period: '2021',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/gebieden-2022-topo.json',
    description:
      'Gebiedsgericht Werken Gebieden sinds de herindeling van 24 maart 2022',
    geography: 'Gebieden Amsterdam',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2015-2020/gebieden-2015-2020-zw-topo.json',
    description:
      'Indeling Gebiedsgericht Werken Gebieden van 2015 tot en met 2020 exclusief waterpartijen',
    geography: 'Gebieden Amsterdam',
    period: '2015-2020',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'stadsdeelCode'],
    colorField: 'stadsdeelCode',
  },
  {
    file: 'amsterdam/2021/gebieden-2021-zw-topo.json',
    description:
      'Tijdelijke Gebiedsgericht Werken Gebieden inclusief Weesp in 2021 exclusief waterpartijen',
    geography: 'Gebieden Amsterdam',
    period: '2021',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/gebieden-2022-zw-topo.json',
    description:
      'Gebiedsgericht Werken Gebieden sinds de herindeling van 24 maart 2022 exclusief waterpartijen',
    geography: 'Gebieden Amsterdam',
    period: '2022',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },
  {
    file: 'amsterdam/2022/gebieden-2022-bewoond-topo.json',
    description:
      'Gebiedsgericht Werken Gebieden sinds de herindeling van 24 maart 2022, alleen bewoonde gebieden',
    geography: 'Gebieden Amsterdam',
    period: '2022',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code', 'stadsdeelNaam'],
    colorField: 'stadsdeelNaam',
  },

  // stadsdelen

  // todo: 1947 - 1954?,
  // todo: 1954 - 1960, zonder westelijke tuinsteden, 8 = landelijk gebied west
  {
    file: 'amsterdam/historisch/stadsdelen-1954-1960-topo.json',
    description: 'Indeling stadsdelen van 1954 tot en met 1960',
    geography: 'Stadsdelen Amsterdam',
    period: '1954-1960',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-1960-1966-topo.json',
    description:
      'Indeling stadsdelen van 1960 tot en met 1966 (met westelijke tuinsteden)',
    geography: 'Stadsdelen Amsterdam',
    period: '1960-1966',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-1966-1986-topo.json',
    description:
      'Indeling stadsdelen van 1966 tot en met 1986 (met bijlmermeer)',
    geography: 'Stadsdelen Amsterdam',
    period: '1966-1986',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-1966-1986-zw-topo.json',
    description:
      'Indeling stadsdelen van 1966 tot en met 1986 exclusief waterpartijen (met bijlmermeer)',
    geography: 'Stadsdelen Amsterdam',
    period: '1966-1986',
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-1986-1998-topo.json',
    description: 'Indeling stadsdelen van 1986 tot en met 1998',
    geography: 'Stadsdelen Amsterdam',
    period: '1986-1998',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-1998-2005-topo.json',
    description: 'Indeling stadsdelen van 1998 tot en met 2005',
    geography: 'Stadsdelen Amsterdam',
    period: '1998-2005',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/historisch/stadsdelen-2005-2015-topo.json',
    description: 'Indeling stadsdelen van 2005 tot en met 2015',
    geography: 'Stadsdelen Amsterdam',
    period: '2005-2015',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'code',
  },
  {
    file: 'amsterdam/2015-2020/stadsdelen-2015-2020-topo.json',
    description: 'Indeling stadsdelen van 2015 tot en met 2020',
    geography: 'Stadsdelen Amsterdam',
    period: '2015-2020',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2021/stadsdelen-2021-topo.json',
    description: 'Tijdelijke indeling stadsdelen inclusief Weesp in 2021',
    geography: 'Stadsdelen Amsterdam',
    period: '2021',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2022/stadsdelen-2022-topo.json',
    description:
      'Indeling stadsdelen en stadsgebied sinds de herindeling van 24 maart 2022',
    geography: 'Stadsdelen Amsterdam',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2015-2020/stadsdelen-2015-2020-zw-topo.json',
    description:
      'Indeling stadsdelen en stadsgebied van 2015 tot en met 2020 exclusief waterpartijen',
    geography: 'Stadsdelen Amsterdam',
    period: '2015-2020',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2021/stadsdelen-2021-zw-topo.json',
    description:
      'Tijdelijke indeling stadsdelen inclusief Weesp in 2021 exclusief waterpartijen',
    geography: 'Stadsdelen Amsterdam',
    period: '2021',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2022/stadsdelen-2022-zw-topo.json',
    description:
      'Indeling stadsdelen en stadsgebied sinds de herindeling van 24 maart 2022 exclusief waterpartijen',
    geography: 'Stadsdelen Amsterdam',
    period: '2022',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'amsterdam/2022/stadsdelen-2022-bewoond-topo.json',
    description:
      'Indeling stadsdelen en stadsgebied sinds de herindeling van 24 maart 2022, alleen bewoonde gebieden',
    geography: 'Stadsdelen Amsterdam',
    period: '2022',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
]

const areasMra = [
  // Buurten MRA
  ...years.map((year) => ({
    file: `mra/${year}/buurten-mra-${year}-topo.json`,
    description: `CBS buurten Metropoolregio Amsterdam ${year}`,
    geography: 'Buurten MRA',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'regioNaam', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  })),
  // Wijken MRA
  ...years.map((year) => ({
    file: `mra/${year}/wijken-mra-${year}-topo.json`,
    description: `CBS wijken Metropoolregio Amsterdam ${year}`,
    geography: 'Wijken MRA',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'regioNaam', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  })),
  // Wijken MRA zonder water
  ...years.map((year) => ({
    file: `mra/${year}/wijken-mra-${year}-zw-topo.json`,
    description: `CBS wijken Metropoolregio Amsterdam ${year} exclusief waterpartijen`,
    geography: 'Wijken MRA',
    period: year.toString(),
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code', 'regioNaam', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  })),
  // Gemeenten MRA
  ...yearsPlus2023.map((year) => ({
    file: `mra/${year}/gemeenten-mra-${year}-topo.json`,
    description: `CBS gemeenten Metropoolregio Amsterdam ${year}`,
    geography: 'Gemeenten MRA',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'regioNaam'],
    colorField: 'naam',
  })),
  // Gemeenten MRA zonder water
  ...yearsPlus2023.map((year) => ({
    file: `mra/${year}/gemeenten-mra-${year}-zw-topo.json`,
    description: `CBS gemeenten Metropoolregio Amsterdam ${year} exclusief waterpartijen`,
    geography: 'Gemeenten MRA',
    period: year.toString(),
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam', 'code', 'regioNaam'],
    colorField: 'naam',
  })),
  // Regios MRA
  ...yearsPlus2023.map((year) => ({
    file: `mra/${year}/regios-mra-${year}-topo.json`,
    description: `Regios Metropoolregio Amsterdam ${year}`,
    geography: 'Regios MRA',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam'],
    colorField: 'naam',
  })),
  // Regios MRA zonder water
  ...yearsPlus2023.map((year) => ({
    file: `mra/${year}/regios-mra-${year}-zw-topo.json`,
    description: `CBS Regios Metropoolregio Amsterdam ${year} exclusief waterpartijen`,
    geography: 'Regios MRA',
    period: year.toString(),
    type: { id: 'zw', label: 'zonder water' },
    tooltipFields: ['naam'],
    colorField: 'naam',
  })),
]

const areasVeiligheidsindex = [
  {
    file: 'veiligheidsindex/gebieden-veiligheidsindex-2022-topo.json',
    description: 'Gebiedsindeling Veiligheidsmonitor 2022',
    geography: 'Gebieden VM',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  },
  {
    file: 'veiligheidsindex/gebieden-veiligheidsindex-2022-zw-topo.json',
    description:
      'Gebiedsindeling Veiligheidsmonitor 2022 exclusief waterpartijen',
    geography: 'Gebieden VM',
    period: '2022',
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  },
]

const areasWiMra = [
  ...[2017, 2019, 2021, 2023].map((year) => ({
    file: `wimra/${year}/gebieden-wimra-${year}-topo.json`,
    description: `Gebiedsindeling Monitor Wonen in de Metropoolregio Amsterdam ${year}`,
    geography: 'Gebieden WiMRA',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  })),
  ...[2017, 2019, 2021, 2023].map((year) => ({
    file: `wimra/${year}/gebieden-wimra-${year}-zw-topo.json`,
    description: `Gebiedsindeling Monitor Wonen in de Metropoolregio Amsterdam ${year} exclusief waterpartijen`,
    geography: 'Gebieden WiMRA',
    period: year.toString(),
    type: {
      id: 'zw',
      label: 'zonder water',
    },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  })),
  {
    file: `wimra/2021/gebieden-wimra-2021-bewoond-topo.json`,
    description: `Gebiedsindeling Monitor Wonen in de Metropoolregio Amsterdam 2021, alleen bewoonde gebieden`,
    geography: 'Gebieden WiMRA',
    period: '2021',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  },
  {
    file: `wimra/2023/gebieden-wimra-2023-bewoond-topo.json`,
    description: `Gebiedsindeling Monitor Wonen in de Metropoolregio Amsterdam 2023, alleen bewoonde gebieden`,
    geography: 'Gebieden WiMRA',
    period: '2023',
    type: {
      id: 'bewoond',
      label: 'bewoond',
    },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'gemeenteNaam',
  },
]

const areasWinkelGebieden = [
  {
    file: 'winkelgebieden/2022/winkelgebieden-2022-topo.json',
    description: 'Winkelgebieden 2022',
    geography: 'Winkelgebieden',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'gebiedCode',
    background: 'amsterdam/2022/gebieden-2022-topo.json',
  },
  {
    file: 'winkelgebieden/2024/winkelgebieden-2024-topo.json',
    description: 'Winkelgebieden 2024',
    geography: 'Winkelgebieden',
    period: '2024',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'categorieNaam'],
    colorField: 'gebiedCode',
    background: 'amsterdam/2022/gebieden-2022-topo.json',
  },
]

const areasArbeidsmarkt = [
  {
    file: 'arbeidsmarkt/gemeenten-arbeidsmarkt-groot-amsterdam-2022-topo.json',
    description: 'Arbeidsmarkt Groot-Amsterdam 2022',
    geography: 'Arbeidsmarkt',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
  {
    file: 'arbeidsmarkt/gemeenten-arbeidsmarkt-groot-amsterdam-2022-zw-topo.json',
    description: 'Arbeidsmarkt Groot-Amsterdam 2022',
    geography: 'Arbeidsmarkt',
    period: '2022',
    type: { id: 'zw', label: 'zonder water gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  },
]

const areasGroengebieden = [
  {
    file: 'groengebieden/groengebieden-2022-topo.json',
    description: 'Groengebieden 2022',
    geography: 'Groengebieden',
    period: '2022',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code', 'naam'],
    colorField: 'stadsdeelCode',
    background: 'amsterdam/2022/stadsdelen-2022-topo.json',
  },
  {
    file: 'groengebieden/groengebieden-2024-topo.json',
    description: 'Groengebieden 2024',
    geography: 'Groengebieden',
    period: '2024',
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['code', 'naam'],
    colorField: 'stadsdeelCode',
    background: 'amsterdam/2022/stadsdelen-2022-topo.json',
  },
]

const municipalitiesNL = [
  ...[
    2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021,
    2022, 2023,
  ].map((year) => ({
    file: `nederland/${year}/gemeenten-cbs-${year}-topo.json`,
    description: `Nederlandse gemeenten ${year} (CBS)`,
    geography: 'Gemeenten Nederland',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code'],
    colorField: 'naam',
  })),
]

const districtsNL = [
  ...[
    2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021,
    2022, 2023,
  ].map((year) => ({
    file: `nederland/${year}/wijken-cbs-${year}-topo.json`,
    description: `Nederlandse wijken ${year} (CBS)`,
    geography: 'Wijken Nederland',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'naam',
  })),
]

const hoodsNl = [
  ...[
    2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021,
    2022, 2023,
  ].map((year) => ({
    file: `nederland/${year}/buurten-cbs-${year}-topo.json`,
    description: `Nederlandse buurten ${year} (CBS)`,
    geography: 'Buurten Nederland',
    period: year.toString(),
    type: { id: '', label: 'hele gebied' },
    tooltipFields: ['naam', 'code', 'gemeenteNaam'],
    colorField: 'naam',
  })),
]

export default [
  ...areasAmsterdam,
  ...areasMra,
  ...areasVeiligheidsindex,
  ...areasWiMra,
  ...areasWinkelGebieden,
  ...areasArbeidsmarkt,
  ...areasGroengebieden,
  ...municipalitiesNL,
  ...districtsNL,
  ...hoodsNl,
]
