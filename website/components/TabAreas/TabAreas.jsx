import { useEffect, useState, useRef } from 'react'

import { prefixUrl } from '../../lib/utils'

import allMaps from './maps'
import styles from './TabAreas.module.css'

function Areas() {
  const [state, setState] = useState({
    geo: 'Buurten Amsterdam',
    period: '2022',
    type: 'zw',
    scheme: 'osMaps',
  })
  const [data, setData] = useState([])
  const {
    geo: currentGeo,
    period: currentPeriod,
    type: currentType,
    scheme: currentScheme,
  } = state
  const mapRef = useRef()

  const availableGeographies = [...new Set(allMaps.map((d) => d.geography))]

  const availablePeriods = [
    ...new Set(
      allMaps.filter((d) => d.geography === currentGeo).map((d) => d.period),
    ),
  ]

  const availableTypes = allMaps
    .filter((d) => d.geography === currentGeo && d.period === currentPeriod)
    .map((d) => d.type)
    .reduce(
      (arr, next) => (arr.find((t) => t.id === next.id) ? arr : [...arr, next]),
      [],
    )

  const currentMap = allMaps.find(
    (d) =>
      d.geography === currentGeo &&
      d.period === currentPeriod &&
      d.type.id === currentType,
  )

  const { file, description, tooltipFields, colorField, background } =
    currentMap

  const topoJsonUrl = prefixUrl(`geo/${file}`)
  const geoJsonUrl = prefixUrl(`geo/${file.replace('-topo', '-geo')}`)
  const currentGeoUrl = currentScheme === 'vega' ? topoJsonUrl : geoJsonUrl

  const specVega = {
    $schema: 'https://vega.github.io/schema/vega-lite/v5.json',
    width: 600,
    height: 500,
    view: { stroke: 'transparent' },
    layer: [
      {
        data: {
          name: 'geodata',
          url: topoJsonUrl,
          format: { type: 'topojson', feature: 'data' },
        },
        mark: {
          type: 'geoshape',
          stroke: 'white',
          strokeWidth: 0.5,
        },
        projection: { type: 'mercator' },
        encoding: {
          color: {
            field: colorField ? `properties.${colorField}` : 'properties.code',
            scale: {
              range: [
                '#a00078',
                '#e50082',
                '#009de6',
                '#fb9bbe',
                '#d48fb9',
                '#a4ccf3',
                '#ffd8e5',
                '#efd2e3',
                '#dceafa',
              ],
            },
            legend: null,
          },
          tooltip: tooltipFields
            ? tooltipFields.map((d) => ({
                title: d,
                field: `properties.${d}`,
              }))
            : [
                { title: 'Naam', field: 'properties.naam' },
                { title: 'Code', field: 'properties.code' },
              ],
        },
      },
    ],
  }

  if (background)
    specVega.layer.unshift({
      data: {
        name: 'backgrounddata',
        url: prefixUrl(`geo/${background}`),
        format: { type: 'topojson', feature: 'data' },
      },
      mark: {
        fill: '#f2f2f2',
        type: 'geoshape',
        stroke: 'white',
        strokeWidth: 1,
      },
      projection: { type: 'mercator' },
    })

  const specOsMaps = {
    $schema:
      'https://onderzoek.amsterdam.nl/static/datavisualisatie-onderzoek-en-statistiek/os-maps.json',
    view: {
      latitude: 52.355,
      longitude: 4.92,
      zoom: 9.9,
      minZoom: 8,
      maxZoom: 12,
    },
    layers: [
      {
        data: [
          {
            name: 'geodata',
            url: geoJsonUrl,
          },
        ],
        mark: 'geoshape',
        encoding: {
          color: {
            field: colorField || 'code',
            scale: {
              type: 'nominal',
              range: [
                '#a00078',
                '#e50082',
                '#009de6',
                '#fb9bbe',
                '#d48fb9',
                '#a4ccf3',
                '#ffd8e5',
                '#efd2e3',
                '#dceafa',
              ],
            },
            legend: false,
          },
          tooltip: tooltipFields
            ? tooltipFields.map((d) => ({
                title: d,
                field: d,
              }))
            : [
                { title: 'Naam', field: 'naam' },
                { title: 'Code', field: 'code' },
              ],
        },
      },
    ],
  }

  const handleFilterChange = (geo, period, type, scheme) => {
    const optionsWithGeo = allMaps.filter((d) => d.geography === geo)
    const newPeriod = optionsWithGeo.find((d) => d.period === period)
      ? period
      : optionsWithGeo[0].period
    const optionsWithGeoAndPeriod = optionsWithGeo.filter(
      (d) => d.period === newPeriod,
    )
    const newType = optionsWithGeoAndPeriod.find((d) => d.type.id === type)
      ? type
      : optionsWithGeoAndPeriod[0].type.id
    setState({
      geo,
      period: newPeriod,
      type: newType,
      scheme,
    })
  }

  const openInEditor = () => {
    mapRef.current.querySelector('.editorButton').click()
  }

  useEffect(() => {
    const getGeoData = async () => {
      const result = await fetch(topoJsonUrl)
      const json = await result.json()
      const geoData = json.objects.data.geometries.map(
        ({ properties }) => properties,
      )
      setData(geoData)
    }
    getGeoData()
  }, [topoJsonUrl])

  const spec = currentScheme === 'vega' ? specVega : specOsMaps

  return (
    <>
      <p className="mb-40">
        Veel kaarten van Onderzoek en Statistiek zijn gebaseerd op een
        gebiedsindeling. Zo is de stad opgedeeld in buurten, wijken, stadsdelen
        en gebieden. Daarnaast zijn er aparte indelingen voor bijvoorbeeld de
        Metropoolregio Amsterdam (MRA), de Veiligheidsindex en de monitor Wonen
        in de MRA (WiMRA). De indelingen kunnen door de tijd veranderen en zijn
        er in verschillende smaken, zoals met en zonder waterpartijen. Hieronder
        staan voorbeelden met links naar de brondata.
      </p>

      <div className={styles.container}>
        <div className={styles.filters}>
          <label>
            Indeling
            <select
              value={currentGeo}
              onChange={(e) =>
                handleFilterChange(
                  e.target.value,
                  currentPeriod,
                  currentType,
                  currentScheme,
                )
              }
            >
              {availableGeographies.map((d) => (
                <option key={d} value={d}>
                  {d}
                </option>
              ))}
            </select>
          </label>
          <label>
            Geldigheid
            <select
              value={currentPeriod}
              onChange={(e) =>
                handleFilterChange(
                  currentGeo,
                  e.target.value,
                  currentType,
                  currentScheme,
                )
              }
            >
              {availablePeriods.map((d) => (
                <option key={d} value={d}>
                  {d}
                </option>
              ))}
            </select>
          </label>
          <label>
            Weergave
            <select
              value={currentType}
              onChange={(e) =>
                handleFilterChange(
                  currentGeo,
                  currentPeriod,
                  e.target.value,
                  currentScheme,
                )
              }
            >
              {availableTypes.map((d) => (
                <option key={d.id} value={d.id}>
                  {d.label}
                </option>
              ))}
            </select>
          </label>
          <label>
            Schema
            <select
              value={currentScheme}
              onChange={(e) =>
                handleFilterChange(
                  currentGeo,
                  currentPeriod,
                  currentType,
                  e.target.value,
                )
              }
            >
              <option value="osMaps">OS-Maps</option>
              <option value="vega">Vega-Lite</option>
            </select>
          </label>

          <button
            className={styles.button}
            type="button"
            data-variant="primary"
            data-small
            onClick={() => openInEditor()}
          >
            <os-icon name="external-link" size="20px" />
            Bewerk in editor
          </button>

          <button
            className={styles.button}
            type="button"
            data-variant="primary"
            data-small
            onClick={() => navigator.clipboard.writeText(currentGeoUrl)}
          >
            <os-icon name="external-link" size="20px" />
            Kopieer geolink
          </button>

          <div
            className={styles.infoBox}
          >{`Totaal: ${data.length} ${currentGeo}`}</div>
        </div>
        <div className={styles.map} ref={mapRef}>
          <os-visualisation
            heading={description}
            source="O&S"
            spec={JSON.stringify(spec)}
          ></os-visualisation>
        </div>
      </div>
    </>
  )
}

export default Areas
