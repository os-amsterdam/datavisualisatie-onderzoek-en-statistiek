import { useState, useEffect } from 'react'
import Editor from '@monaco-editor/react'

import { prefixUrl, cx, debounce } from '../../lib/utils'

import styles from './TabEditor.module.css'

const schemaUrl = prefixUrl('os-maps.json')

const examples = {
  basis: 'basis.json',
  filters: 'filters.json',
  points: 'points.json',
  heatmap: 'heatmap.json',
  'arcmap regions mra': 'arcmap-regions-mra.json',
  'arcmap municpalities mra': 'arcmap-municipalities-mra.json',
  'arcmap municpalities nl': 'arcmap-municipalities-nl.json',
  'nominal scale': 'scale-nominal.json',
  'theshold scale': 'scale-threshold.json',
  'cluster scale': 'scale-clusters.json',
  'linear scale': 'scale-linear.json',
  text: 'text.json',
}

const addMarkdownProps = (value) => {
  if (typeof value === 'object' && value !== null) {
    if (value.description) {
      value.markdownDescription = value.description
    }

    for (const key in value) {
      // eslint-disable-next-line no-prototype-builtins
      if (value.hasOwnProperty(key)) {
        value[key] = addMarkdownProps(value[key])
      }
    }
  }
  return value
}

function SpecEditor() {
  const [schema, setSchema] = useState({})
  const [spec, setSpec] = useState('')
  const [example, setExample] = useState('')
  const [errors, setErrors] = useState([])
  const [showMapAboveEditor, setShowMapAboveEditor] = useState(false)

  // check spec validity and update spec
  const handleSpecUpdate = (json) => {
    try {
      JSON.parse(json)
      setSpec(json)
    } catch (e) {
      setErrors(['No valid json', e.message])
    }
  }

  // change example
  const handleChange = async (exampleName) => {
    if (!exampleName) {
      setExample('')
    } else {
      setExample(exampleName)
      setErrors([])
      const path = examples[exampleName]
      const url = prefixUrl(`specs/maps/${path}`)
      const result = await fetch(url)
      const json = await result.json()
      // replace schema with schema for this env
      // json.$schema = schemaUrl
      handleSpecUpdate(JSON.stringify(json, null, ' '))
    }
  }

  const getSchema = async () => {
    const result = await fetch(schemaUrl)
    setSchema(await result.json())
  }

  useEffect(() => {
    //load first example and schema
    getSchema()

    // check for spec in local storage
    const jsonSpec = sessionStorage.getItem('spec')
    if (jsonSpec) {
      handleSpecUpdate(jsonSpec)
      sessionStorage.removeItem('spec')
      handleChange('')
    } else {
      handleChange('basis')
    }
  }, [])

  const initEditor = (monaco) => {
    monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
      allowComments: true,
      enableSchemaRequest: true,
      validate: true,
      schemas: [{ uri: schemaUrl, schema: addMarkdownProps(schema) }],
    })

    monaco.languages.json.jsonDefaults.setModeConfiguration({
      documentFormattingEdits: true,
      documentRangeFormattingEdits: false,
      completionItems: true,
      hovers: true,
      documentSymbols: true,
      tokens: true,
      colors: true,
      foldingRanges: true,
      diagnostics: true,
    })

    monaco.editor.onDidChangeMarkers(([uri]) => {
      const markers = monaco.editor.getModelMarkers({ resource: uri })
      setErrors(markers.map(({ message }) => message))
    })
  }

  return (
    <>
      <p className="mb-40">
        Vega-lite ondersteunt geen kaarten met achtergrond-<i>tiles</i> en
        soepel <i>pannen</i> en <i>zoomen</i>. Op de website van Onderzoek en
        Statistiek gebruiken we voor dat soort kaarten een component dat een op
        Vega-lite gebaseerde specificatie gebruikt. Met deze editor kun je die
        specificaties maken.
      </p>
      <div className={styles.buttons}>
        <select
          value={example}
          onChange={(e) => handleChange(e.target.value)}
          className="mb-20"
        >
          <option value="" disabled>
            Kies een voorbeeld
          </option>
          {Object.keys(examples).map((key) => (
            <option key={key} value={key}>
              {key}
            </option>
          ))}
        </select>
        <button
          data-variant="secondary"
          data-small
          onClick={() => setShowMapAboveEditor(!showMapAboveEditor)}
        >
          ↺
        </button>
      </div>

      <div
        className={cx(
          styles.container,
          showMapAboveEditor ? styles.mapAbove : styles.mapNext,
        )}
      >
        <div className={styles.editorPane}>
          <Editor
            height="900px"
            defaultLanguage="json"
            value={spec}
            onChange={debounce(500, handleSpecUpdate)}
            beforeMount={(monaco) => initEditor(monaco)}
            options={{
              cursorBlinking: 'smooth',
              folding: true,
              lineNumbersMinChars: 4,
              minimap: { enabled: false },
              scrollBeyondLastLine: false,
              wordWrap: 'on',
              quickSuggestions: true,
              tabSize: 2,
            }}
          />
        </div>
        <div className={styles.mapPane}>
          {spec && (
            <os-visualisation
              spec={spec}
              heading="Voorbeeld"
            ></os-visualisation>
          )}
          {errors.length > 0 && (
            <div className={styles.error}>
              {errors.map((err, i) => (
                <div key={err + i}>{err}</div>
              ))}
            </div>
          )}
        </div>
      </div>
    </>
  )
}

export default SpecEditor
