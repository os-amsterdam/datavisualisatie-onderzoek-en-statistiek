import stringify from 'json-stringify-pretty-compact'

import config from './specConfig'

// open spec in vega or map editor
// based on https://github.com/vega/vega-embed/blob/next/src/post.ts
export function postSpec(window, url, data) {
  const editor = window.open(url)
  const wait = 10000
  const step = 250
  let count = ~~(wait / step)

  function listen(evt) {
    if (evt.source === editor) {
      count = 0
      window.removeEventListener('message', listen, false)
    }
  }
  window.addEventListener('message', listen, false)

  // periodically resend message until ack received or timeout
  function send() {
    if (count <= 0) return
    editor.postMessage(data, '*')
    setTimeout(send, step)
    count -= 1
  }
  setTimeout(send, step)
}

const getJson = async (spec) => {
  if (typeof spec === 'string') {
    const result = await fetch(spec)
    return await result.json()
  } else {
    return spec
  }
}

// open editor
export const openEditor = async (spec) => {
  const json = await getJson(spec)

  let type
  if (json.$schema.includes('vega')) type = 'vega'
  if (json.$schema.includes('vega-lite')) type = 'vega-lite'
  if (json.$schema.includes('os-map')) type = 'os-map'

  if (type.includes('vega')) {
    postSpec(window, 'https://vega.github.io/editor/', {
      mode: type,
      renderer: 'svg',
      spec: stringify(json),
      config,
    })
  }

  if (type === 'os-map') {
    sessionStorage.setItem('spec', stringify(json))
    const url = prefixParentUrl('?tab=editor')
    window.open(url)
  }
}

const host = window.location.hostname

export const prefixUrl = (path) => {
  const base =
    'onderzoek.amsterdam.nl/static/datavisualisatie-onderzoek-en-statistiek/'
  if (host === 'localhost') return `${window.location.origin}/${path}`
  if (host.startsWith('acc')) return `https://acc.${base}${path}`
  return `https://${base}${path}`
}

export const prefixParentUrl = (path) => {
  const base = 'onderzoek.amsterdam.nl/interactief/datavisualisatie'
  if (host === 'localhost')
    return `${window.location.origin}/iframe.html${path}`
  if (host.startsWith('acc')) return `https://acc.${base}${path}`
  return `https://${base}${path}`
}

// concat class names
export const cx = (...args) =>
  args
    .flat()
    .filter((x) => typeof x === 'string')
    .join(' ')
    .trim()

export const debounce = (wait, callback) => {
  let timeoutId = null
  return (...args) => {
    window.clearTimeout(timeoutId)
    timeoutId = window.setTimeout(() => {
      callback.apply(null, args)
    }, wait)
  }
}
