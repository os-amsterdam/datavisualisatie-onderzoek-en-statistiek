import { createRoot } from 'react-dom/client'

import App from './components/App/App'

const container = document.getElementById('root')
const root = createRoot(container)
root.render(<App />)

// setup app to work well in iframe
const resizeObserver = new ResizeObserver((entries) => {
  entries.forEach((entry) =>
    window.parent.postMessage(
      {
        type: 'frameHeightChanged',
        payload: entry.contentRect.height,
      },
      '*',
    ),
  )
})

window.addEventListener('load', () => {
  if (window.top !== window.self) {
    document.body.style.overflow = 'hidden'
    resizeObserver.observe(container)
  }
})
