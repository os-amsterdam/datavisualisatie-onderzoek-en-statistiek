## Modules

<dl>
<dt><a href="#module_Helper functions">Helper functions</a></dt>
<dd></dd>
</dl>

## Classes

<dl>
<dt><a href="#OSVisualisation">OSVisualisation</a></dt>
<dd><p>Custom Web Component for rendering OS visualisations.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#renderBaseHtml">renderBaseHtml(component)</a></dt>
<dd><p>Renders the base HTML in the given component.</p>
</dd>
<dt><a href="#renderBindings">renderBindings(component)</a></dt>
<dd><p>Renders the bindings for a map visualisation</p>
</dd>
<dt><a href="#initDeckInstance">initDeckInstance(component)</a></dt>
<dd><p>Initializes a new Deck.gl instance within the given component.</p>
</dd>
<dt><a href="#renderVega">renderVega(component)</a></dt>
<dd><p>Renders a Vega visualisation</p>
</dd>
<dt><a href="#renderVisualisation">renderVisualisation(component)</a></dt>
<dd><p>Renders the visualisation, either an image, a Vega chart, or a map.</p>
</dd>
<dt><a href="#handleType">handleType(component, container, editorButton)</a></dt>
<dd><p>Handles rendering based on the component type.</p>
</dd>
<dt><a href="#addBindingListeners">addBindingListeners(component)</a></dt>
<dd><p>Adds event listeners to binding elements inside the component.</p>
</dd>
</dl>

<a name="module_Helper functions"></a>

## Helper functions

* [Helper functions](#module_Helper functions)
    * [.colorToRgb](#module_Helper functions.colorToRgb) ⇒ <code>array</code>
    * [.formatValue](#module_Helper functions.formatValue) ⇒ <code>string</code>
    * [.getDefaultParams](#module_Helper functions.getDefaultParams)
    * [.createColorScale](#module_Helper functions.createColorScale)
    * [.saveAs](#module_Helper functions.saveAs)
    * [.jsonToCsv](#module_Helper functions.jsonToCsv)
    * [.slugify](#module_Helper functions.slugify)
    * [.handleTitleInSpec](#module_Helper functions.handleTitleInSpec)
    * [.getType](#module_Helper functions.getType)
    * [.parseSpec](#module_Helper functions.parseSpec)
    * [.filterData](#module_Helper functions.filterData) ⇒ <code>Array</code> \| <code>null</code>

<a name="module_Helper functions.colorToRgb"></a>

### Helper functions.colorToRgb ⇒ <code>array</code>
Translates a hexadecimal color to rgb

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
**Returns**: <code>array</code> - Color in rgb  

| Param | Type | Description |
| --- | --- | --- |
| color | <code>string</code> | Color in hex format |

<a name="module_Helper functions.formatValue"></a>

### Helper functions.formatValue ⇒ <code>string</code>
Formats a number to Dutch locale

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
**Returns**: <code>string</code> - Formatted number  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>number</code> |  |
| format | <code>string</code> | The d3 format |

<a name="module_Helper functions.getDefaultParams"></a>

### Helper functions.getDefaultParams
Extracts parameters from a spec

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.createColorScale"></a>

### Helper functions.createColorScale
Creates a color scale

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.saveAs"></a>

### Helper functions.saveAs
Saves a data url to a file

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.jsonToCsv"></a>

### Helper functions.jsonToCsv
Converts json to csv

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.slugify"></a>

### Helper functions.slugify
Converts a string into a slug
Based on https://byby.dev/js-slugify-string

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.handleTitleInSpec"></a>

### Helper functions.handleTitleInSpec
Checks if the spec containt a title or subtitle

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.getType"></a>

### Helper functions.getType
Checks he type of the spec

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.parseSpec"></a>

### Helper functions.parseSpec
Parses a spec

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
<a name="module_Helper functions.filterData"></a>

### Helper functions.filterData ⇒ <code>Array</code> \| <code>null</code>
Filters out keys that start with an underscore and removes empty objects.

**Kind**: static constant of [<code>Helper functions</code>](#module_Helper functions)  
**Returns**: <code>Array</code> \| <code>null</code> - - Filtered dataset or null if empty.  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Array</code> | The dataset to filter. |

<a name="OSVisualisation"></a>

## OSVisualisation
Custom Web Component for rendering OS visualisations.

**Kind**: global class  

* [OSVisualisation](#OSVisualisation)
    * [.connectedCallback()](#OSVisualisation+connectedCallback)
    * [.disconnectedCallback()](#OSVisualisation+disconnectedCallback)
    * [.emitClickEvent(data, id)](#OSVisualisation+emitClickEvent)
    * [.emitBindingChange(event, id)](#OSVisualisation+emitBindingChange)
    * [.attributeChangedCallback(name, oldValue, newValue)](#OSVisualisation+attributeChangedCallback)

<a name="OSVisualisation+connectedCallback"></a>

### osVisualisation.connectedCallback()
Called when the component is added to the DOM.

**Kind**: instance method of [<code>OSVisualisation</code>](#OSVisualisation)  
<a name="OSVisualisation+disconnectedCallback"></a>

### osVisualisation.disconnectedCallback()
Called when the component is removed from the DOM.

**Kind**: instance method of [<code>OSVisualisation</code>](#OSVisualisation)  
<a name="OSVisualisation+emitClickEvent"></a>

### osVisualisation.emitClickEvent(data, id)
Emits a custom event when a visualisation is clicked.

**Kind**: instance method of [<code>OSVisualisation</code>](#OSVisualisation)  

| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Clicked data. |
| id | <code>String</code> | The id of the component that was clicked. |

<a name="OSVisualisation+emitBindingChange"></a>

### osVisualisation.emitBindingChange(event, id)
Emits a custom event when a user changes a binding value.

**Kind**: instance method of [<code>OSVisualisation</code>](#OSVisualisation)  

| Param | Type | Description |
| --- | --- | --- |
| event | <code>Event</code> | The change event. |
| id | <code>String</code> | The id of the component that was clicked. |

<a name="OSVisualisation+attributeChangedCallback"></a>

### osVisualisation.attributeChangedCallback(name, oldValue, newValue)
Called when an observed attribute changes.

**Kind**: instance method of [<code>OSVisualisation</code>](#OSVisualisation)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The attribute name. |
| oldValue | <code>string</code> | The previous value. |
| newValue | <code>string</code> | The new value. |

<a name="renderBaseHtml"></a>

## renderBaseHtml(component)
Renders the base HTML in the given component.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |

<a name="renderBindings"></a>

## renderBindings(component)
Renders the bindings for a map visualisation

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |

<a name="initDeckInstance"></a>

## initDeckInstance(component)
Initializes a new Deck.gl instance within the given component.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The component containing the map. |

<a name="renderVega"></a>

## renderVega(component)
Renders a Vega visualisation

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |

<a name="renderVega..handleClickEvent"></a>

### renderVega~handleClickEvent(_, item)
Event listener for Vega click events.

**Kind**: inner method of [<code>renderVega</code>](#renderVega)  

| Param | Type | Description |
| --- | --- | --- |
| _ | <code>Event</code> | Unused event object. |
| item | <code>Object</code> | Clicked item data. |

<a name="renderVisualisation"></a>

## renderVisualisation(component)
Renders the visualisation, either an image, a Vega chart, or a map.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |

<a name="handleType"></a>

## handleType(component, container, editorButton)
Handles rendering based on the component type.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |
| container | <code>HTMLElement</code> \| <code>null</code> | The container element. |
| editorButton | <code>HTMLElement</code> \| <code>null</code> | The editor button. |

<a name="addBindingListeners"></a>

## addBindingListeners(component)
Adds event listeners to binding elements inside the component.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>HTMLElement</code> | The visualisation component. |

