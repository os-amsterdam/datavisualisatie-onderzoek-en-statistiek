# OS Visualisation Web Component

This web component renders vega-visualisations and interactive maps and is used on onderzoek.amsterdam.nl and other Onderzoek en Statsitiek projects.

## Basic example

```html
<os-visualisation
  heading="Loop van de bevolking"
  source="O&amp;S"
  caption="Uitleg over deze grafiek"
  spec="../specs/vega/stackedBarWithLine.vg.json"
>
</os-visualisation>
```

More examples can be found on [this page](https://onderzoek.amsterdam.nl/static/datavisualisatie-onderzoek-en-statistiek/os-vis-component.html).

## Attributes

| name        | description                                                                                                             |
| ----------- | ----------------------------------------------------------------------------------------------------------------------- |
| `heading`   | The title of the visualisation, can be overruled with `title` in the spec.                                              |
| `spec`      | A specification as a json string or an url to a json file. Can be either a vega-lite, an os-maps or a panel group spec. |
| `caption`   | A caption to be discplayed underneath the chart or map.                                                                 |
| `source`    | The source of the data, displayed in the footer of the visualisation.                                                   |
| `no-footer` | Boolean. If true, no footer will be rendered.                                                                           |
| `no-border` | Boolean. If true, no border will be rendered.                                                                           |
| `no-table`  | Boolean. If true, no table will be rendered.                                                                            |

## Events

The component emmits custom events when a user clicks on the chart or map of changes a parameter.
| name | description |
|---|---|
| `os-vis-click` | An event containing the `slug` of the visualisation and a `datum` object with values for the clicked chart or map element. |
| `os-vis-binding-change` | An event containing the `visId` of the visualisation, the `type` of the element (`select-one`, `range` or `checkbox`), the `name` and the `value` of the parameter. |

## Spec

- If a `title` is part of the spec, it has prevelance above the `heading` attribute.
- If a `subtitle` is part of the spec, it will be rendered below the title of the visualisation.
- If a dataset in the spec is named `table`, a button will be rendered in the footer that toggles a html table. Variales starting with an underscore (`_`) are not shown in the table.
