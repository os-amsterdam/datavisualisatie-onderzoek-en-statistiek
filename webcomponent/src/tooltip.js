import { formatValue } from './utils.js'

export const getTooltip = ({ layer, object }, layerData) => {
  const encoding = layerData?.find(({ id }) => id === layer?.id)?.encoding

  return (
    object &&
    encoding &&
    encoding.tooltip && {
      html: `<div class="tooltip">
        <table>
          <tbody>
            ${encoding.tooltip
              .filter(({ field }) => object[field] != null)
              .map(
                ({ title, field, format }) =>
                  `<tr>
                  <td>${title || field}</td>
                  <td>${formatValue(object[field], format)}</td>
                </tr>`,
              )
              .join('')}
          </tbody>
        </table>
      </div>`,
    }
  )
}
