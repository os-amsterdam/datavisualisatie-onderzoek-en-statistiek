import renderVisualisation from './visualisation.js'
import renderButtons from './buttons'
import { slugify, getType, parseSpec } from './utils.js'

// render a single panel
export const renderPanel = async (panelId, component) => {
  // update meta data, we use heading as key because title could be changed by spec
  component.metaData = component.panelGroup.panels.find(
    ({ id }) => id === panelId,
  )

  // remove existing bindings
  component.querySelector('.bindings').innerHTML = ''

  // clear data
  component.data = null

  // parse spec and get type for this panel
  await parseSpec(component)
  getType(component)

  //add transition if needed
  if (component.panelGroup.transition === 'fade') {
    const container = component.querySelector('.container')
    container.classList.add('fadeIn')
    setTimeout(() => container.classList.remove('fadeIn'), 500)
  }

  // render visualisation for this panel
  renderVisualisation(component)
}

const panelGroup = (component) => {
  // keep panel spec for future reference and meta data
  component.panelGroup = component.metaData.parsedSpec

  // check for missing headings, add slugs and ids
  component.panelGroup.panels = component.panelGroup.panels
    .map((panel, i) => ({
      ...panel,
      id: `panel-${i + 1}`,
      heading: panel.heading || `visualisatie ${i + 1}`,
      subtitle: panel.heading || '',
      title: component.metaData.title,
    }))
    .map((panel) => ({
      ...panel,
      slug: slugify(panel.heading),
    }))

  const { selectorType, selectorLabel, panels } = component.panelGroup
  const controls = component.querySelector('.controls')
  const panelSelector = document.createElement('label')
  const label = selectorLabel || 'Toon'

  if (selectorType === 'buttons') {
    renderButtons(component, 'panel')
  } else {
    const panelTitles = panels.map(
      ({ id, heading }) =>
        `<option class='panel-option' value="${id}">${heading}</option>`,
    )
    panelSelector.innerHTML = `${label} <select>${panelTitles}</select>`
  }
  controls.prepend(panelSelector)

  // add event listeners to selector for panel group
  const handlePanelSelector = (e) => {
    const panelId = e.target.value
    component.panelGroup.current = panelId
    renderPanel(panelId, component)
  }
  const select = panelSelector.querySelector('select')
  select?.addEventListener('change', handlePanelSelector)

  // render first panel, it
  renderPanel(component.panelGroup.panels[0].id, component)
}

export default panelGroup
