import { updateLayerData } from './updateLayerData.js'
import { updateDeckLayers } from './updateDeckLayers.js'

const getBindingsHtml = (specification, params) =>
  specification?.layers
    ?.map((d) =>
      d.params
        ?.filter(({ bind }) => bind)
        .map(({ bind, name }) => {
          if (bind.input === 'select') {
            return `
              <label>
                <span>${bind?.name || name}</span>
                <select name="${name}">
                  ${bind.options
                    ?.map(
                      (option, i) =>
                        `<option value="${option}" ${
                          option === params[name] ? 'selected' : ''
                        }>
                          ${bind.labels ? bind.labels[i] : option}
                        </option>`,
                    )
                    .join('')}
                </select>
              </label>`
          }

          if (bind.input === 'range') {
            return `
              <label>
                <span>${bind?.name || name} <span class="value">(${params[name]})</span></span>
                <input
                  type="range"
                  value="${params[name]}"
                  name="${name}"
                  min="${bind.min}"
                  max="${bind.max}"
                  step="${bind.step || 1}"
                  width="200"
                />
              </label>`
          }

          return null
        })
        .filter(Boolean) // Remove null values
        .join(''),
    )
    .join('') || ''

/**
 * Renders the bindings for a map visualisation
 * @param {HTMLElement} component -  The visualisation component.
 */
const renderBindings = (component) => {
  const specification = component.metaData.parsedSpec
  const { params } = component.state

  const updateLayersOnParamChange = async (event) => {
    try {
      const { name, value } = event.target
      const newParams = {
        ...params,
        // force numbers
        [name]: Number.isNaN(+value) ? value : +value,
      }
      const layerData = await updateLayerData(specification.layers, newParams)
      const layers = updateDeckLayers(layerData, params)

      component.state = { ...component.state, params, layerData, layers }
      component.deckInstance.setProps({ layers })
    } catch (error) {
      console.error('Error updating layers:', error)
    }
  }

  // Handle bindings if params exist
  if (params && Object.keys(params).length > 0) {
    const bindingsContainer = component.querySelector('.bindings')
    if (bindingsContainer) {
      bindingsContainer.innerHTML = getBindingsHtml(specification, params)

      bindingsContainer
        .querySelectorAll('select')
        .forEach((node) =>
          node.addEventListener('change', updateLayersOnParamChange),
        )

      bindingsContainer
        .querySelectorAll('input[type="range"]')
        .forEach((node) =>
          node.addEventListener('input', (event) => {
            node.parentElement.querySelector('.value').textContent =
              `(${node.value})`
            updateLayersOnParamChange(event)
          }),
        )
    }
  }
}

export default renderBindings
