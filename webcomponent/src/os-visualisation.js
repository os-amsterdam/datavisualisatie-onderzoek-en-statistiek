import renderBaseHtml from './base.js'
import renderPanelGroup from './panel-group.js'
import renderVisualisation from './visualisation.js'
import renderFooter from './footer.js'
import editorButton from './editor.js'
import { slugify, parseSpec, getType } from './utils.js'

import './os-visualisation-style.css'

/**
 * Custom Web Component for rendering OS visualisations.
 */
class OSVisualisation extends HTMLElement {
  static observedAttributes = [
    'class',
    'heading',
    'caption',
    'source',
    'spec',
    'buttons',
  ]

  /**
   * Called when the component is added to the DOM.
   */
  async connectedCallback() {
    this.metaData = {
      title: this.getAttribute('heading') || '',
      caption: this.getAttribute('caption') || '',
      source: this.getAttribute('source'),
      imageUrl: this.getAttribute('imageUrl'),
      altText: this.getAttribute('altText') || '',
      className: this.getAttribute('class') || '',
      spec: this.getAttribute('spec'),
      slug: slugify(this.getAttribute('heading')) || 'visualisatie',
      noFooter: this.hasAttribute('no-footer'),
      noBorder: this.hasAttribute('no-border'),
      noTable: this.hasAttribute('no-table'),
    }

    // Define namespace if it's not yet available
    window.osVisualisation ||= {}

    await parseSpec(this)
    getType(this)

    if (this.type) {
      renderBaseHtml(this)
      this.type === 'panels'
        ? renderPanelGroup(this)
        : renderVisualisation(this)
    } else {
      console.warn('No valid type for visualisation:', this.metaData.title)
    }

    // (Re)initialize the editor button
    editorButton(this)
  }

  /**
   * Called when the component is removed from the DOM.
   */
  disconnectedCallback() {
    this.vegaInstance?.finalize()
    this.deckInstance?.finalize()
  }

  /**
   * Emits a custom event when a visualisation is clicked.
   * @param {Object} data - Clicked data.
   * @param {String} id - The id of the component that was clicked.
   */
  emitClickEvent(data, id) {
    this.dispatchEvent(
      new CustomEvent('os-vis-click', {
        bubbles: true,
        cancelable: true,
        detail: {
          visId: id,
          datum: data.datum || data.object || data,
        },
      }),
    )
  }

  /**
   * Emits a custom event when a user changes a binding value.
   * @param {Event} event - The change event.
   * @param {String} id - The id of the component that was clicked.
   */
  emitBindingChange(event, id) {
    const { type, name, value, checked } = event.target
    this.dispatchEvent(
      new CustomEvent('os-vis-binding-change', {
        bubbles: true,
        cancelable: true,
        detail: {
          visId: id,
          type,
          name,
          value: type === 'checkbox' ? checked : value,
        },
      }),
    )
  }

  /**
   * Called when an observed attribute changes.
   * @param {string} name - The attribute name.
   * @param {string} oldValue - The previous value.
   * @param {string} newValue - The new value.
   */
  async attributeChangedCallback(name, oldValue, newValue) {
    if (!this.metaData || oldValue === newValue) return

    switch (name) {
      case 'heading': {
        const heading = this.querySelector('h2')
        if (heading) heading.innerHTML = newValue
        this.metaData.title = newValue
        this.metaData.slug = slugify(newValue)
        break
      }

      case 'caption': {
        const caption = this.querySelector('figcaption')
        if (caption) caption.innerHTML = newValue
        this.metaData.caption = newValue
        break
      }

      case 'source': {
        const source = this.querySelector('.source')
        if (source) source.innerHTML = newValue
        this.metaData.source = newValue
        break
      }

      case 'spec':
        this.metaData.spec = newValue
        await parseSpec(this)
        getType(this)

        if (this.type === 'map' && this.deckInstance) {
          const { renderMap } = window.osVisualisation.osMap
          await renderMap(this)
          renderFooter(this)
        } else {
          this.deckInstance = null
          renderVisualisation(this)
        }
        break
    }
  }
}

window.customElements.define('os-visualisation', OSVisualisation)
