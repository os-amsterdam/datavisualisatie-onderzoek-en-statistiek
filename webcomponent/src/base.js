/**
 * Renders the base HTML in the given component.
 * @param {HTMLElement} component -  The visualisation component.
 */
const renderBaseHtml = (component) => {
  const { className, noFooter, noBorder } = component.metaData

  component.innerHTML = `
      <article class="${className}${noBorder ? ' noBorder' : ''}">
      <button class="editorButton" data-small>bewerk</button>
        <hgroup>
          <h2 data-style-as='h5'></h2>
          <p data-small></p>
        </hgroup>
        <figure>
          <div class="static"></div>
          <div class="container"></div>
          <figcaption></figcaption>
        </figure>
        <div class="controls">
        <div class="bindings"></div></div>
        ${noFooter ? '' : '<footer></footer>'}
      </article>
      `
}

export default renderBaseHtml
