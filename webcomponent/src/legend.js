import { color as d3Color } from 'd3'

import { createColorScale, formatValue } from './utils.js'

const propToCssVar = (fillColor) => {
  const bgColor =
    d3Color(fillColor) && d3Color(fillColor).copy({ opacity: 0.5 })
  return bgColor ? `--bg-color: ${bgColor}` : ''
}

const gradient = (direction, start, end) =>
  `linear-gradient(${
    direction === 'vertical' ? 'to bottom' : 'to right'
  }, ${start} , ${end})`

const legendList = ({ items, title, fillColor, orient, direction }) => {
  return `<div class="legend" style="${propToCssVar(fillColor)}" data-variant="list" data-orient="${orient}">
     <div>${title || ''}</div>
      <ul>
      ${items
        .map(
          ({ color, value }) =>
            `<li style="--color: ${color}" data-direction=${direction}>${value}</li>`,
        )
        .join('')}
      </ul>
    </div>`
}

const legendBreakPoints = ({
  items,
  title,
  fillColor,
  orient,
  direction,
  format,
}) => {
  const sortedItems =
    direction === 'horizontal' ? items : items.slice().reverse()

  return `<div class="legend" style="${propToCssVar(fillColor)}" data-variant="break-points" data-orient="${orient}" data-direction=${direction}>
      <div>${title || ''}</div>
      <div data-direction="${direction}">${sortedItems
        .map(({ color }) => `<div style="--color: ${color}"></div>`)
        .join('')}
      </div>
      <div data-direction=${direction}>
        ${sortedItems
          .filter(({ value }) => value)
          .map(
            ({ value }) => `<div >${value && formatValue(value, format)}</div>`,
          )
          .join('')}
      </div>
    </div>`
}

const legendGradient = ({
  items,
  title,
  fillColor,
  orient,
  direction,
  format,
}) => {
  return `<div class="legend" style="${propToCssVar(fillColor)}" data-variant="gradient" data-orient="${orient}" data-direction="${direction}">
    <div>${title || ''}</div>
    <div data-direction=${direction}>
        ${items.map(({ value }) => `<div>${formatValue(value, format)}</div>`).join('')}
    </div>
    <div data-direction="${direction}" style="--gradient: ${gradient(direction, items[0].color, items[1].color)}" />
    </div>`
}

const getLegend = (legend) => {
  const { type } = legend
  switch (type) {
    case 'nominal':
    case 'ordinal':
      return legendList(legend)
    case 'threshold':
    case 'clusters':
      return legendBreakPoints(legend)
    case 'linear':
      return legendGradient(legend)
    default:
      return null
  }
}

export const getLegends = (layers) => {
  if (!layers) return ''
  const legends = layers
    .filter((layer) => layer?.encoding?.color?.legend)
    .map((layer) => {
      const type = layer.encoding.color?.scale?.type
      const allowedTypes = [
        'nominal',
        'ordinal',
        'threshold',
        'clusters',
        'linear',
      ]
      if (!allowedTypes.includes(type)) return
      const colorScale = createColorScale(layer, 'color')
      let values = colorScale.domain()
      if (colorScale.clusters) values = colorScale.clusters()
      const { orient, direction } = layer.encoding.color.legend

      return {
        id: layer.name,
        type,
        items: colorScale
          .range()
          .map((color, i) => ({ color, value: values[i] })),
        orient: orient || 'bottom-left',
        direction: direction || 'vertical',
        ...layer.encoding.color.legend,
      }
    })
    .filter((d) => d)

  return legends && legends.map((legend) => getLegend(legend))
}
