import { saveAs, jsonToCsv } from './utils.js'

const sanitize = (data) => {
  // if data is a number, turn it into a string in the Dutch notation
  // the thousands seperator is removed here, otherwise years (e.g. 2014) would also get a seperator
  if (typeof data === 'number') {
    return data.toLocaleString('nl-NL', { useGrouping: false })
  }
  // if data is a date, parse it and return a string
  if (data instanceof Date) {
    return data.toLocaleDateString('nl-NL')
  }
  // if data is not a js primitive (so an object, array, etc.), return null
  if (typeof data === 'object') {
    return null
  }
  return data
}

const getFooterHtml = (source, data) => {
  const headers = data && Object.keys(data[0])

  const table = data
    ? `
        <table>
        <thead>
          <tr>
            ${headers.map((item) => `<th>${sanitize(item)}</th>`).join('')}
          </tr>
        </thead>
        <tbody>
          ${data
            .map(
              (row) =>
                `<tr>${headers.map((col) => `<td>${sanitize(row[col])}</td>`).join('')}</tr>`,
            )
            .join('')}
        </tbody>
      </table>`
    : ''

  return `
    <div>
        <p data-small class="source">${source ? `Bron: ${source}` : ''}</p>
        <div class="options">
        <button data-small class="downloadImageButton" aria-label="Download afbeelding">
          <os-icon name="download" size="20"></os-icon>
          <os-icon name="spinner" size="20"></os-icon>
          Download afbeelding
          </button>
        ${data ? `<button data-small class="showDataButton" aria-label="Toon cijfers in tabel"><os-icon name="chevron-down" size="20"></os-icon>Toon cijfers</button>` : ''}
        </div>
    </div> 
    ${
      data
        ? `
      <div class="dataTable">
        <div>${table}</div>
        <div>
          <button data-small class="downloadDataButton" aria-label="Download cijfers">
            <os-icon name="download" size="20"></os-icon>Download cijfers
          </button>
        </div>
      </div>`
        : ''
    }
    `
}

const footer = (component) => {
  if (component.querySelector('footer')) {
    if (component.metaData.noTable) component.data = null
    component.footer = component.querySelector('footer')
    component.footer.innerHTML = getFooterHtml(
      component.metaData.source,
      component.data,
    )

    // add event listner to show data button
    component.showData = false
    component
      .querySelector('.showDataButton')
      ?.addEventListener('click', () => {
        component.showData = !component.showData
        if (component.showData) component.footer.classList.add('showData')
        else component.footer.classList.remove('showData')
      })

    // add event listner to download data button
    component
      .querySelector('.downloadDataButton')
      ?.addEventListener('click', () => {
        const csvData = jsonToCsv(component.data)
        saveAs(
          URL.createObjectURL(new Blob([csvData])),
          `${component.metaData.slug}.csv`,
        )
      })

    // add event listner to download imgage button
    component
      .querySelector('.downloadImageButton')
      ?.addEventListener('click', async () => {
        if (!window.osVisualisation.osDownload) {
          window.osVisualisation.osDownload = await import('./download.js')
        }

        // get state for static image
        if (component.type === 'vega') {
          component.state = component.vegaInstance.view.getState()
        }

        const { downloadImage } = window.osVisualisation.osDownload

        downloadImage({
          component: component,
          state: component.state,
          ...component.metaData,
        })
      })
  }
}

export default footer
