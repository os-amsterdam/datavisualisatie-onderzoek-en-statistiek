import { TileLayer } from '@deck.gl/geo-layers'
import {
  BitmapLayer,
  GeoJsonLayer,
  ScatterplotLayer,
  ArcLayer,
  TextLayer,
} from '@deck.gl/layers'
import { HeatmapLayer } from '@deck.gl/aggregation-layers'

import { colorToRgb } from './utils'

export const updateDeckLayers = (layers = [], params) => {
  const tiles = new TileLayer({
    id: 'tiles',
    data: [
      `https://cartodb-basemaps-a.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`,
      `https://cartodb-basemaps-b.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`,
      `https://cartodb-basemaps-c.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`,
      `https://cartodb-basemaps-d.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`,
    ],
    maxZoom: 19,
    minZoom: 0,
    pickable: false,

    renderSubLayers: (props) => {
      const { boundingBox } = props.tile

      return new BitmapLayer(props, {
        data: null,
        image: props.data,
        bounds: [
          boundingBox[0][0],
          boundingBox[0][1],
          boundingBox[1][0],
          boundingBox[1][1],
        ],
      })
    },
    maxRequests: 20,
    tileSize: 256,
    transparentColor: [255, 255, 255, 255],
  })

  const updatedLayers = [tiles]

  // loop overspec layers and convert them to Deck layers
  for (const layer of layers) {
    const { mark, id, layerData, encoding } = layer

    // geojson layers
    if (mark === 'geoshape' || mark?.type === 'geoshape') {
      updatedLayers.push(
        new GeoJsonLayer({
          id,
          data: layerData,
          pickable: encoding?.tooltip !== undefined,
          autoHighlight: true,
          opacity: mark?.opacity || encoding?.opacity || 0.8,
          highlightColor: [0, 0, 0, 50],
          stroked: true,
          filled: true,
          lineWidthMinPixels:
            mark?.strokeWidth !== undefined ? mark.strokeWidth : 1,
          getLineColor:
            mark?.stroke !== undefined
              ? colorToRgb(mark.stroke)
              : [0, 0, 0, 100],

          getFillColor: (d) => d.color || [0, 0, 0, 0],
          updateTriggers: { getFillColor: [params, layerData] },
        }),
      )
    }

    // point layers
    if (mark === 'points' || mark?.type === 'points') {
      updatedLayers.push(
        new ScatterplotLayer({
          id,
          data: layerData,
          pickable: encoding?.tooltip !== undefined,
          stroked: true,
          filled: true,
          radiusScale: 10,
          radiusMinPixels: 3,
          radiusMaxPixels: 100,
          opacity: mark?.opacity || encoding?.opacity || 0.8,
          lineWidthMinPixels:
            mark?.strokeWidth !== undefined ? mark.strokeWidth : 0,

          getLineColor:
            mark?.stroke !== undefined
              ? colorToRgb(mark.stroke)
              : [0, 0, 0, 100],
          getPosition: (d) => [+d.longitude, +d.latitude],
          getRadius: (d) => d.size,
          getFillColor: (d) => d.color || [0, 0, 0, 0],
          updateTriggers: { getFillColor: [params, layerData] },
        }),
      )
    }

    // heatmap layers
    if (mark === 'heatmap' || mark?.type === 'heatmap') {
      updatedLayers.push(
        new HeatmapLayer({
          id,
          data: layerData,
          pickable: false,
          opacity: mark?.opacity || encoding?.opacity || 0.8,
          colorRange: encoding?.color?.scale?.range?.map(colorToRgb) || [
            [255, 243, 167, 100],
            [246, 189, 87, 150],
            [242, 125, 20, 200],
            [236, 0, 0],
          ],
          colorDomain: encoding?.color?.scale?.domain || null,
          getPosition: (d) => [+d.longitude, +d.latitude],
          intensity: 1,
          threshold: 0.03,
          radiusPixels: 30,
          updateTriggers: {
            colorRange: [params, layerData],
            colorDomain: [params, layerData],
            radiusPixels: [params, layerData],
          },
        }),
      )
    }

    // arcmap layers
    if (mark === 'arcmap' || mark?.type === 'arcmap') {
      updatedLayers.push(
        new ArcLayer({
          id,
          data: layerData,
          pickable: encoding?.tooltip !== undefined,
          greatCircle: true,
          opacity: mark?.opacity || encoding?.opacity || 1,
          getSourcePosition: (d) => d.fromCoordinates,
          getTargetPosition: (d) => d.toCoordinates,
          getSourceColor: (d) => {
            if (d.color) return [d.color[0], d.color[1], d.color[2], 100]
            return [(0, 0, 0, 100)]
          },
          getTargetColor: (d) => {
            if (d.color) return d.color
            return [(0, 0, 0)]
          },
          getWidth: (d) => d.size,
          updateTriggers: {
            getSourcePosition: [params, layerData],
            getSourceColor: [params, layerData],
            getWidth: [params, layerData],
          },
        }),
      )
    }

    // arcmap layers
    if (mark === 'text' || mark?.type === 'text') {
      updatedLayers.push(
        new TextLayer({
          id,
          data: layerData,
          pickable: encoding?.tooltip !== undefined,
          opacity: mark?.opacity || encoding?.opacity || 1,
          fontFamily: 'Amsterdam Sans, arial, sans-serif',
          fontWeight: mark?.fontWeight || 'normal',
          background: mark?.background || false,
          backgroundPadding: [4, 2, 4, 0],
          getText: (d) => d.text,
          getPosition: (d) => d.coordinates,
          getColor: (d) => {
            // if no color is encoded, loof for mark.fill or use black
            if (!d.color) d.color = colorToRgb(mark?.fill) || [0, 0, 0, 255]
            return [d.color[0], d.color[1], d.color[2], 255]
          },
          getSize: (d) => d.size || mark.size || 16,
          updateTriggers: {
            getSourcePosition: [params, layerData],
            getSourceColor: [params, layerData],
            getWidth: [params, layerData],
          },
        }),
      )
    }
  }

  return updatedLayers
}
