import { renderPanel } from './panel-group'

/**
 * Renders stepper buttons in the given component.
 * @param {HTMLElement} component -  The visualisation component.
 * @param {stepperType} string  -  Type of stepper, 'panel' or 'spec'
 */
const renderButtons = (component, stepperType = 'spec') => {
  const buttonsInSpec = component.metaData.parsedSpec?.params?.find(
    ({ name }) => name === 'buttons',
  )?.value

  const numberOfButtons =
    stepperType === 'panel' ? component.panelGroup.panels.length : buttonsInSpec

  if (!numberOfButtons) return

  // add stepper buttons to controls
  const controlsElement = component.querySelector('.controls')
  const buttonContainer = document.createElement('div')
  buttonContainer.classList.add('stepperButtons')
  const buttonsHtml = [...Array(+numberOfButtons).keys()]
    .map(
      (i) =>
        `<button 
              class='stepperButton'
              data-variant="${i === 0 ? 'primary' : 'secondary'}" 
              data-small 
              value="${i + 1}">${i + 1}
            </button>`,
    )
    .join('')
  const nextButton = `<button 
      class='stepperButton next' data-variant="secondary" data-small 
    ><os-icon name="chevron-right"></os-icon></button>`
  buttonContainer.innerHTML = buttonsHtml + nextButton
  controlsElement.prepend(buttonContainer)

  // add event listener to buttons
  const stepperButtonContainer = component.querySelector('.stepperButtons')
  stepperButtonContainer.value = 1

  const handleStepperButton = (e) => {
    const clickedElement = e.target
    if (clickedElement.tagName.toLowerCase() === 'button') {
      const currentStep = stepperButtonContainer.value
      let nextStep
      if (clickedElement.classList.contains('next')) {
        nextStep = currentStep === +numberOfButtons ? 1 : currentStep + 1
      } else {
        nextStep = +clickedElement.value
      }

      stepperButtonContainer.value = nextStep

      component.querySelectorAll('.stepperButton').forEach((btn) => {
        if (+btn.value === nextStep) btn.dataset.variant = 'primary'
        else btn.dataset.variant = 'secondary'
      })

      if (stepperType === 'panel') {
        component.panelGroup.current = nextStep
        renderPanel(`panel-${nextStep}`, component)
      }
    }
  }
  stepperButtonContainer.addEventListener('click', handleStepperButton)
}

export default renderButtons
