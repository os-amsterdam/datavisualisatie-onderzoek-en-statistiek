import vegaEmbed from 'vega-embed'
import renderFooter from './footer'
import config from '../../website/lib/specConfig'
import { filterData } from './utils'

// Export vegaEmbed for use elsewhere (i.e. fixed width visualsiation for download)
// we only need it there in case of a vega vis and don't want it to
// clutter the bundle in case of we are rendering an image or a map
export { vegaEmbed }

/**
 * @function renderVega
 * @description Renders a Vega visualisation
 * @param {HTMLElement} component - The visualisation component.
 */
export const renderVega = async (component) => {
  try {
    const { parsedSpec: specification } = component.metaData
    const bindings = component.querySelector('.bindings')
    const container = component.querySelector('.container')

    // Define Vega options
    const options = {
      config,
      renderer: 'svg',
      actions: false,
      bind: bindings,
    }

    // Hide the Vega SVG from screen readers
    container.setAttribute('aria-hidden', 'true')

    // Render Vega visualisation
    component.vegaInstance = await vegaEmbed(container, specification, options)

    /**
     * Event listener for Vega click events.
     * @param {Event} _ - Unused event object.
     * @param {Object} item - Clicked item data.
     */
    const handleClickEvent = (_, item) => {
      if (item?.datum) {
        component.emitClickEvent(item.datum, component.id)
      }
    }

    // Add event listener for custom click events
    component.vegaInstance.view.addEventListener('click', handleClickEvent)

    // Allow Vega chart to scale by removing fixed width/height
    const svgElement = container.firstChild
    if (svgElement) {
      svgElement.removeAttribute('width')
      svgElement.removeAttribute('height')
    }

    // Get Vega state and check if dataset "table" exists
    const vegaState = component.vegaInstance.view.getState({
      data: () => true,
      signals: () => false,
      recurse: true,
    })

    if ('table' in vegaState.data) {
      // Set initial data and update when dataset changes
      component.data = filterData(vegaState.data.table)

      component.vegaInstance.view.addDataListener('table', (_, value) => {
        component.data = filterData(value)
        renderFooter(component)
      })
    }
  } catch (error) {
    console.error('Error rendering Vega visualisation:', error)
  }
}
