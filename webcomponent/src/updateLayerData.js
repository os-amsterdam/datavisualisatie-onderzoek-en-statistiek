import { csv, tsv, dsv, extent, autoType, scaleLinear } from 'd3'

import { colorToRgb, createColorScale } from './utils'

const logError = (error) => console.warn(error)

const fetchData = async (url, type = 'json', delimiter = ';') => {
  try {
    if (type === 'json') return await fetch(url).then((result) => result.json())
    if (type === 'csv') return await csv(url, autoType)
    if (type === 'tsv') return await tsv(url, autoType)
    if (type === 'dsv') return await dsv(delimiter, url, autoType)
  } catch (error) {
    logError(error)
  }
  return null
}

const operatorsFunctions = {
  '||': (a, b) => a || b,
  '&&': (a, b) => a && b,
  '===': (a, b) => a === b,
  '==': (a, b) => a == b,
  '<=': (a, b) => a <= b,
  '>=': (a, b) => a >= b,
  '<': (a, b) => a < b,
  '>': (a, b) => a > b,
  '!==': (a, b) => a !== b,
}

const applyFilter = (filter, params, datum) => {
  // split filter string in logical parts between && and ||
  const regex = /&&|\|\|/gi
  const operators = filter.match(regex)
  const parts = filter.split(regex)

  // determine value for each parts
  const partValues = parts.map((part) => {
    const members = part.trim().split(' ')
    const operator = members[1]
    const left = datum[members[0].replace('datum.', '')]
    const right =
      params[members[2]?.replaceAll("'", '')] || members[2]?.replaceAll("'", '')

    // check if operator is supported
    if (!operatorsFunctions[operator]) {
      throw new Error(`Unsupported operator: ${operator}`)
    }
    return operatorsFunctions[operator](left.toString(), right.toString())
  })

  // reduce part values to single value based on operators
  const value = partValues.reduce(
    (acc, curr, i) =>
      i === 0 ? acc : operatorsFunctions[operators[i - 1]](curr, acc),
    partValues[0],
  )

  return value
}

// TODO: use vega-expression?
// https://github.com/vega/vega/tree/main/packages/vega-expression
const transformData = (values, transform, params) => {
  let data = values
  for (const transformation of transform) {
    if (transformation.filter) {
      data = data.filter((d) => applyFilter(transformation.filter, params, d))
    }
  }
  return data
}

const encodeColor = (layer) => {
  const { layerData, encoding } = layer
  const { field, value, scale } = encoding.color
  // derive the color straight from the data
  if (scale === null || (scale?.type === 'declarative' && field)) {
    return layerData.map((d) => ({
      ...d,
      color: colorToRgb(d[field]),
    }))
  }
  // create color scale
  const colorScale = createColorScale(layer, 'color')
  return layerData.map((d) => ({
    ...d,
    color: value ? colorToRgb(value) : colorToRgb(colorScale(d[field])),
  }))
}

const encodeSize = (layer) => {
  const { layerData, encoding } = layer
  const { scale, field, value } = encoding.size
  const range = scale?.range || [0, 100]
  const values = layerData.map((d) =>
    Number.isNaN(+d[field]) ? d[field] : +d[field],
  )
  const domain = scale?.domain ? extent(scale?.domain) : extent(values)
  const sizeScale = scaleLinear().domain(domain).range(range)

  return layerData.map((d) => ({
    ...d,
    size: value || sizeScale(d[field]),
  }))
}

const encodeText = (layer) => {
  const { layerData, encoding } = layer
  const { field } = encoding.text

  return layerData.map((d) => ({
    ...d,
    text: d[field],
  }))
}

export const updateLayerData = async (layers, params) => {
  if (!layers) return

  const updatedLayers = []

  // general catch so spec errors don't break the page
  try {
    // loop over layers
    // Deck needs a unique id for each layer, so we need an index
    // therefor we use for ... in instead of for ... of

    for (const layerNumber in layers) {
      const layer = layers[layerNumber]

      const mark = layer.mark.type || layer.mark
      layer.id = `${layer.name || 'layer'}-${+layerNumber + 1}`
      // loop over data sources and fetch if necessary
      for (const dataSource of layer.data) {
        const { url, values, type, delimiter } = dataSource

        // no values yet? fetch them
        // TODO: fetch data synchronously
        dataSource.values = !values
          ? await fetchData(url, type, delimiter)
          : values

        // geojson? reshape data
        if (dataSource.values.features) {
          dataSource.values = dataSource.values.features
            .filter((d) => d) // get rid of empty features
            .map(({ type: t, geometry, properties }) => ({
              type: t,
              geometry,
              ...properties,
            }))
        }

        // apply data transforms
        dataSource.transformed =
          dataSource.transform && dataSource.transform.length > 0
            ? transformData(dataSource.values, dataSource.transform, params)
            : dataSource.values

        // add key for joining data
        dataSource.transformed = dataSource.transformed.map((d) => ({
          ...d,
          joinKey: d[dataSource.key],
        }))
      }

      // join data
      layer.layerData = layer.data.reduce(
        (total, dataSource) =>
          total.length === 0
            ? dataSource.transformed
            : total.map((d) => ({
                ...d,
                ...dataSource.transformed.find(
                  // force numeric keys to strings
                  ({ joinKey }) => joinKey.toString() === d.joinKey.toString(),
                ),
              })),
        [],
      )

      // apply data transform on joined data
      if (layer.transform && layer.transform.length > 0) {
        layer.layerData = transformData(
          layer.layerData,
          layer.transform,
          params,
        )
      }

      // in case of arcmap, create sourcePosition and targetPosition objects
      if (mark === 'arcmap') {
        // create lookup for centroids
        const refData = layer.data.filter(
          (dataSource) => dataSource.values[0]?.centroid,
        )[0]
        const dataWithCentroids = {}
        refData?.values.forEach((d) => {
          dataWithCentroids[d[refData.key]] = d.centroid
        })

        // check if  'to' and 'from' fields contain coordinates. Else, lookup coordinates based on key
        layer.layerData = layer.layerData.map((d) => {
          const fromCoordinates = Array.isArray(d.from)
            ? d.from
            : dataWithCentroids[d.from]
          const toCoordinates = Array.isArray(d.to)
            ? d.to
            : dataWithCentroids[d.to]
          return {
            ...d,
            fromCoordinates,
            toCoordinates,
          }
        })
      }

      // encode color if needed
      if (layer?.encoding?.color && mark !== 'heatmap') {
        layer.layerData = encodeColor(layer)
      }
      // encode size if needed
      if (layer?.encoding?.size && mark !== 'heatmap') {
        layer.layerData = encodeSize(layer)
      }

      // encode text if needed
      if (layer?.encoding?.text && mark === 'text') {
        layer.layerData = encodeText(layer)
      }

      updatedLayers.push({
        ...layer,
        layerData: layer.layerData,
      })
    }

    return updatedLayers
  } catch (error) {
    logError(error)
  }
}
