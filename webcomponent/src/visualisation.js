import renderImage from './image.js'
import renderButtons from './buttons'
import renderFooter from './footer.js'

/**
 * Renders the visualisation, either an image, a Vega chart, or a map.
 * @param {HTMLElement} component - The visualisation component.
 */
const renderVisualisation = async (component) => {
  const { title = '', subtitle = '', caption = '' } = component.metaData

  // Create elements references
  const h2 = component.querySelector('hgroup h2')
  const p = component.querySelector('hgroup p')
  const figcaption = component.querySelector('figcaption')
  const container = component.querySelector('.container')
  const editorButton = component.querySelector('.editorButton')

  // Update title, subtitle, and caption if elements exist
  if (h2) h2.innerHTML = title
  if (p) p.innerHTML = subtitle
  if (figcaption) figcaption.innerHTML = caption

  // Handle different visualisation types
  await handleType(component, container, editorButton)

  // Remove min-height to prevent layout shift
  component.style.minHeight = 'unset'
}

/**
 * Handles rendering based on the component type.
 * @param {HTMLElement} component - The visualisation component.
 * @param {HTMLElement|null} container - The container element.
 * @param {HTMLElement|null} editorButton - The editor button.
 */
const handleType = async (component, container, editorButton) => {
  if (!component.type) return

  if (component.type === 'image') {
    renderImage(component)
    renderFooter(component)
    return
  }

  if (component.type === 'vega' || component.type === 'map') {
    // add loading state to vis container, but only if loading takes more than 500 ms
    let loadingTimeout
    if (container) {
      loadingTimeout = setTimeout(() => container.classList.add('loading'), 500)
    }

    // render stepper buttons if there are any
    // we need to do this before the spec is rendered so Vega can create the bindings
    renderButtons(component, 'spec')

    if (component.type === 'vega') {
      // only import vega if we need it, and only import it once
      if (!window.osVisualisation.osVis) {
        window.osVisualisation.osVis = await import('./os-vega.js')
      }
      await window.osVisualisation.osVis.renderVega(component)
    }

    if (component.type === 'map') {
      // only import code for os maps if we need it, and only import it once
      if (!window.osVisualisation.osMap) {
        window.osVisualisation.osMap = await import('./os-maps.js')
      }
      await window.osVisualisation.osMap.renderMap(component)
    }

    renderFooter(component)

    // Set up event listeners for input bindings
    addBindingListeners(component)

    // Remove loading state
    if (loadingTimeout) clearTimeout(loadingTimeout)
    if (container) container.classList.remove('loading')

    // Ensure the editor button is visible if needed
    if (editorButton) editorButton.classList.remove('alwaysHide')
  }
}

/**
 * Adds event listeners to binding elements inside the component.
 * @param {HTMLElement} component - The visualisation component.
 */
const addBindingListeners = (component) => {
  const emitSignalEvent = (event) =>
    component.emitBindingChange(event, component?.id)

  component
    .querySelectorAll(
      '.bindings select, .bindings input[type="range"], .bindings input[type="checkbox"]',
    )
    .forEach((node) => node.addEventListener('input', emitSignalEvent))
}

export default renderVisualisation
