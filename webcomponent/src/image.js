const renderImage = (component) => {
  const { imageUrl, altText } = component.metaData

  // render image tag with width and height with 16:9 aspect ratio
  // this minimizes layout shift
  const container = component.querySelector('.container')
  container.innerHTML = `<img src="${imageUrl}" alt="${altText}" width="640" height="360"></img>`

  // always hide editor button for images
  component.querySelector('.editorButton').classList.add('alwaysHide')
}

export default renderImage
