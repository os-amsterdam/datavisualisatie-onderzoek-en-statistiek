// open spec in vega editor
// based on   https://github.com/vega/vega-embed/blob/next/src/post.ts
const postSpec = (window, url, data) => {
  const editor = window.open(url)
  const wait = 10000
  const step = 250
  let count = ~~(wait / step)

  function listen(evt) {
    if (evt.source === editor) {
      count = 0
      window.removeEventListener('message', listen, false)
    }
  }
  window.addEventListener('message', listen, false)

  // periodically resend message until ack received or timeout
  function send() {
    if (count <= 0) return
    editor.postMessage(data, '*')
    setTimeout(send, step)
    count -= 1
  }
  setTimeout(send, step)
}

const prefixMapEditorUrl = (path) => {
  const host = window.location.hostname
  const base = 'onderzoek.amsterdam.nl/interactief/datavisualisatie'
  if (host === 'localhost')
    return `${window.location.origin}/iframe.html${path}`
  if (host.startsWith('acc')) return `https://acc.${base}${path}`
  return `https://${base}${path}`
}

const handleButtonClick = (component) => {
  const { type } = component
  const { spec } = component.metaData

  // be sure to send a nicely formatted string tot the editor
  const encodedSpec =
    typeof spec === 'object'
      ? JSON.stringify(spec, null, '  ')
      : JSON.stringify(JSON.parse(spec), null, '  ')

  if (type === 'vega') {
    postSpec(window, 'https://vega.github.io/editor/', {
      mode: type,
      renderer: 'svg',
      spec: encodedSpec,
    })
  }

  if (type === 'map') {
    sessionStorage.setItem('spec', encodedSpec)
    const url = prefixMapEditorUrl('?tab=editor')
    window.open(url)
  }

  // copy spec to clipboard
  // navigator.clipboard.writeText(spec)
}

const editorButon = (component) => {
  // add listner for edito button
  const editorButton = component.querySelector('.editorButton')
  editorButton?.addEventListener(
    'click',
    () => handleButtonClick(component),
    false,
  )

  // listen for E key, but only once for each document
  if (!window.osVisualisation.showEditorButton) {
    window.osVisualisation.showEditorButton = true
    document.addEventListener('keydown', (event) => {
      if (event.key === 'E') {
        document
          .querySelectorAll('.editorButton')
          .forEach((element) => element.classList.toggle('show'))
      }
    })
  }
}

export default editorButon
