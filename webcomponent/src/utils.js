/** @module Helper functions */

import {
  formatLocale,
  color as d3Color,
  extent,
  scaleOrdinal,
  scaleLinear,
  scaleThreshold,
} from 'd3'
import scaleCluster from 'd3-scale-cluster'

/**
 * Translates a hexadecimal color to rgb
 * @param {string} color Color in hex format
 * @returns {array} Color in rgb
 */
export const colorToRgb = (color) => {
  if (Array.isArray(color)) return color
  const rgb = d3Color(color)
  return rgb ? [rgb.r, rgb.g, rgb.b, rgb.opacity * 255] : [0, 0, 0, 0]
}

/**
 * Formats a number to Dutch locale
 * @param {number} value
 * @param {string} format The d3 format
 * @returns {string} Formatted number
 */
export const formatValue = (value, format = ',') => {
  const nlLocale = {
    decimal: ',',
    thousands: '.',
    grouping: [3],
    dateTime: '%a %e %B %Y %X',
    date: '%d-%m-%Y',
    time: '%H:%M:%S',
  }
  const locale = formatLocale(nlLocale)

  // catch invalid formatting strings
  try {
    return Number.isNaN(+value) ? value : locale.format(format)(+value)
  } catch {
    return value
  }
}

/** Extracts parameters from a spec */
export const getDefaultParams = (specification) =>
  specification.layers
    ? specification.layers
        .flatMap((layer) => layer.params)
        .reduce(
          (obj, param) =>
            param
              ? {
                  ...obj,
                  [param.name]: param.value,
                }
              : obj,
          {},
        )
    : {}

/** Creates a color scale */
export const createColorScale = (layer) => {
  const { layerData, encoding } = layer
  const { scale, field } = encoding.color
  const values = layerData
    ? layerData.map((d) => (Number.isNaN(+d[field]) ? d[field] : +d[field]))
    : [0, 1]

  const nominalColors = [
    '#004699',
    '#009dec',
    '#6cbd74',
    '#bed200',
    '#ffe600',
    '#fdb0cb',
    '#d48fb9',
    '#ff9100',
    '#ec0000',
  ]

  const ordinalColors = ['#d5d7eb', '#aab0d6', '#7f8ac2', '#5167ad', '#004699']

  // no scale in spec? create one
  const defaultType =
    layerData && Number.isNaN(+layerData[0]?.[field]) ? 'nominal' : 'linear'
  const type = scale?.type || defaultType
  if (!scale) encoding.color.scale = {}
  if (!encoding.color.scale.type) encoding.color.scale.type = defaultType

  // no domain or range in spec? define defaults
  const rangeOrdinal =
    scale?.range && scale.range.length > 0 ? scale?.range : ordinalColors
  const rangeLinear = [rangeOrdinal[0], rangeOrdinal[rangeOrdinal.length - 1]]
  const domainNominal = scale?.domain || [...new Set(values)]
  const domainThresholds =
    scale?.domain ||
    scaleCluster().domain(values).range(rangeOrdinal).clusters()

  const domainLinear = scale?.domain ? extent(scale.domain) : extent(values)
  const rangeNominal =
    scale?.range || domainNominal.map((v, i) => nominalColors[i])

  // sort domain and range if domain is reversed
  if (domainThresholds[0] > domainThresholds[domainThresholds.length - 1]) {
    domainThresholds.reverse()
    rangeOrdinal.reverse()
  }

  switch (type) {
    case 'nominal':
      return scaleOrdinal().domain(domainNominal).range(rangeNominal)
    case 'ordinal':
      return scaleOrdinal().domain(domainNominal).range(rangeOrdinal)
    case 'threshold':
      return scaleThreshold().domain(domainThresholds).range(rangeOrdinal)
    case 'clusters':
      return scaleCluster().domain(values).range(rangeOrdinal)
    case 'linear':
      return scaleLinear().domain(domainLinear).range(rangeLinear)
    default:
      return null
  }
}

/** Saves a data url to a file */
export const saveAs = (uri, filename) => {
  const link = document.createElement('a')
  link.href = uri
  link.download = filename
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
}

/** Converts json to csv */
export const jsonToCsv = (json, delimiter = ',') => {
  const header = Object.keys(json[0])
  const headerString = header.join(',')
  // handle null or undefined values here
  const replacer = (key, value) => value ?? ''
  const rowItems = json.map((row) =>
    header
      .map((fieldName) => JSON.stringify(row[fieldName], replacer))
      .join(delimiter),
  )
  // join header and body, and break into separate lines
  const csv = [headerString, ...rowItems].join('\r\n')
  return csv
}

/** Converts a string into a slug
 * Based on https://byby.dev/js-slugify-string
 */
export const slugify = (str) => {
  if (!str) return
  return String(str)
    .normalize('NFKD')
    .replace(/[\u0300-\u036f]/g, '')
    .trim()
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, '')
    .replace(/\s+/g, '-')
    .replace(/-+/g, '-')
}

/** Checks if the spec containt a title or subtitle */
export const handleTitleInSpec = (component) => {
  const { parsedSpec } = component.metaData
  const specTitle = parsedSpec.title
  if (!specTitle) return

  const title = specTitle?.text || specTitle
  const subTitle = specTitle?.subtitle || ''
  const panelSubTitle = `${title} ${subTitle || ''}`

  if (!title || typeof title !== 'string') return

  // update meta data title, subtitle and slug
  // panels keep the title of the panel group
  if (component.hasPanels) {
    component.metaData.subtitle = panelSubTitle
    component.metaData.slug = slugify(panelSubTitle)
  } else {
    component.metaData.title = title
    component.metaData.subtitle = subTitle
    component.metaData.slug = slugify(title)
  }

  delete parsedSpec.title
}

/** Checks he type of the spec */
export const getType = (component) => {
  if (component.metaData.imageUrl) component.type = 'image'
  if (component.metaData.parsedSpec) {
    const schema = component.metaData.parsedSpec.$schema
    if (schema?.includes('vega')) component.type = 'vega'
    if (schema?.includes('os-maps')) component.type = 'map'
  }
  if (component.metaData.parsedSpec?.panels) {
    component.type = 'panels'
    // set extra flag because component.type can be overwritten by panel
    component.hasPanels = true
  }
}

/** Parses a spec */
export const parseSpec = async (component) => {
  if (!component.metaData.spec) return

  try {
    // allready parsed?
    if (typeof component.metaData.spec === 'object') {
      // make parsed spec a copy of spec instead of a reference
      component.metaData.parsedSpec = JSON.parse(
        JSON.stringify(component.metaData.spec),
      )
      handleTitleInSpec(component)
      return
    }
    // do we have a spec that is an url? fetch it
    if (component.metaData.spec.endsWith('.json')) {
      const response = await fetch(component.metaData.spec)
      component.metaData.spec = await response.text()
    }
    const jsonSpec = JSON.parse(component.metaData.spec)

    component.metaData.parsedSpec = jsonSpec
    handleTitleInSpec(component)
  } catch {
    console.warn(
      'No valid specification for visualisation:',
      component.metaData.title,
    )
  }
}

/**
 * Filters out keys that start with an underscore and removes empty objects.
 * @param {Array} data - The dataset to filter.
 * @returns {Array|null} - Filtered dataset or null if empty.
 */
export const filterData = (data) => {
  if (!Array.isArray(data)) return null

  const filteredData = data
    .map((item) =>
      Object.fromEntries(
        Object.entries(item).filter(([key]) => !key.startsWith('_')),
      ),
    )
    .filter((item) => Object.keys(item).length > 0)

  return filteredData.length > 0 ? filteredData : null
}
