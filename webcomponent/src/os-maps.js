import { Deck } from '@deck.gl/core'

import { updateLayerData } from './updateLayerData.js'
import { updateDeckLayers } from './updateDeckLayers.js'
import { getLegends } from './legend.js'
import { getTooltip } from './tooltip.js'
import renderBindings from './bindings.js'
import { getDefaultParams } from './utils.js'

// export for use elsewhere (i.e. fixed width map for download and map update)
// we only need it there in case of a os map and don't want it to
// clutter the bundle in case of we are rendering an image or a Vega visualisation
export { Deck, getLegends, updateDeckLayers }

// Default map settings
const defaultView = {
  latitude: 52.355,
  longitude: 4.92,
  zoom: 9.9,
  minZoom: 9.5,
  maxZoom: 12,
  bearing: 0,
  pitch: 0,
  transitionDuration: 250,
}

/**
 * Initializes a new Deck.gl instance within the given component.
 * @param {HTMLElement} component - The component containing the map.
 */
const initDeckInstance = async (component) => {
  try {
    // define container and ad class
    const container = component.querySelector('.container')
    container.classList.add('deck')

    // get values from meta data
    const { altText, parsedSpec: specification } = component.metaData

    // define container height
    const canvasHeight = specification?.height || 500
    container.style.minHeight = `${canvasHeight}px`

    // create markup
    container.innerHTML = `
    <canvas class="responsive">${altText}</canvas>
    <div class="legends" aria-label="legenda"></div>
    <div class="buttons">
      <button type="button" title="Zoom in" aria-label="Zoom in">+</button>
      <button type="button" title="Zoom uit" aria-label="Zoom uit">-</button>
    </div>
  </div>`

    const map = component.querySelector('canvas.responsive')

    // get map data from state
    const { layers, initialViewState } = component.state

    // init and render deck.gl instance
    component.deckInstance = new Deck({
      initialViewState,
      canvas: map,
      controller: {
        scrollZoom: false,
        touchZoom: false,
      },
      layers,
      getTooltip: (data) => getTooltip(data, component.state.layerData),
      getCursor: ({ isDragging, isHovering }) => {
        if (isDragging) return 'grabbing'
        if (isHovering) return 'pointer'
        return 'grab'
      },
      // on view change, update map state
      onViewStateChange: (obj) => {
        component.state = {
          ...component.state,
          initialViewState: obj.viewState,
        }
      },
      // on click, emit custom click event
      onClick: (d) => component.emitClickEvent(d, component?.id),
    })

    // add event listner to zoom buttons
    component.querySelector('.buttons').addEventListener('click', (e) => {
      const step = e.target.textContent === '+' ? 1 : -1
      component.deckInstance.setProps({
        initialViewState: {
          ...component.state.initialViewState,
          zoom: component.state.initialViewState.zoom + step,
          transitionDuration: 250,
        },
      })
    })
  } catch (error) {
    console.error('Error initializing Deck.gl instance:', error)
  }
}

/** @function
 * Renders or updates the Deck.gl visualization.
 * @param {HTMLElement} component - The component to render the map in.
 */
export const renderMap = async (component) => {
  try {
    const specification = component.metaData.parsedSpec

    // create or update view state
    const pitch = specification?.view?.pitch || 0
    const bearing = specification?.view?.bearing || 0
    const initialViewState = {
      ...defaultView,
      ...component?.state?.initialViewState,
      ...specification?.view,
      pitch,
      bearing,
    }

    // create or update layers state
    const params = getDefaultParams(specification)
    const layerData = await updateLayerData(specification.layers, params)
    const layers = updateDeckLayers(layerData, params)
    const legendContainer = component.querySelector('.legends')

    component.state = { params, layerData, layers, initialViewState }

    // no Deck instance yet? Or no longer a container for legends? Create new Deck Instance
    if (!component.deckInstance || !legendContainer) {
      await initDeckInstance(component)
    }

    // add or update layers en view state
    component.deckInstance.setProps({ layers, initialViewState })

    // add legend(s)
    component.querySelector('.legends').innerHTML = getLegends(layerData)

    // define map data
    // check alle data souces for all layers and return the first one with the name 'table'
    component.data = layerData
      ?.flatMap((layer) => layer.data)
      ?.find(({ name }) => name === 'table')?.values

    renderBindings(component)
  } catch (error) {
    console.error('Error rendering Deck.gl map:', error)
  }
}
