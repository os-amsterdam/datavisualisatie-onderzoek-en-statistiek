// import html2canvas from 'html2canvas'
import { toPng } from 'html-to-image'

import config from '../../website/lib/specConfig.js'

import { saveAs } from './utils.js'

// fetch image and translate it to data url
const toDataURL = (url) =>
  fetch(url)
    .then((response) => response.blob())
    .then(
      (blob) =>
        new Promise((resolve, reject) => {
          const reader = new FileReader()
          reader.onloadend = () => resolve(reader.result)
          reader.onerror = reject
          reader.readAsDataURL(blob)
        }),
    )

// find all values for the name key of bindings in a vega-lite spec
export const findBindings = (obj) => {
  let result = []

  function recurse(current) {
    if (typeof current === 'object' && current !== null) {
      // Check if the current object has both 'name' and 'bind' keys
      if ('name' in current && 'bind' in current) {
        // use a a label either a specific label, or a the label for the input element or the param name
        const { name, bind, value } = current
        const { options, labels } = bind || {}
        const label = bind?.label || bind?.name || name

        result.push({ key: name, label, defaultValue: value, options, labels })
      }
      for (let key in current) {
        // Recurse through nested objects
        recurse(current[key])
      }
    }
  }

  recurse(obj)
  return result
}

// format param value
const parseParamValue = (value, options, labels) => {
  // check if we need tot translate select options to labels
  if (options && labels) {
    // some people use arrays for option values, so to find the index, we need to coerse the values to strings
    const index = options.findIndex((d) => d.toString() === value.toString())
    return labels[index]
  }

  // param bind with input element
  if (typeof value === 'string' || typeof value === 'number') return value
  if (typeof value === 'boolean' && value === true) return 'ja'

  // param bind with legend
  if (typeof value === 'object') {
    const values = Object.values(value)
      .filter((d) => Array.isArray(d))
      .flat()
    if (values.length === 1) return values[0]
    return values.join(', ')
  }
  return
}

// render param values as a list
const renderParamList = (parsedSpec, params, element) => {
  const bindingNames = findBindings(parsedSpec)
  if (bindingNames.length === 0) return
  const bindingData = bindingNames
    .filter(({ key }) => key !== 'step') // don't shop stepper bindings
    .map(({ key, label, defaultValue, options, labels }) => ({
      key,
      label,
      value:
        parseParamValue(params[key], options, labels) ||
        defaultValue?.toString(),
    }))
    .filter(({ value }) => value)

  // add current binding values
  element.querySelector('.params').innerHTML = `<dl>
    ${bindingData
      .map(({ label, value }) => `<dt>${label}:</dt><dd>${value}</dd>`)
      .join('')}
      </d>`
}

const removeNotForDownloadLayers = (obj) => {
  // if the object is not an object or array, return.
  if (typeof obj !== 'object' || obj === null) return

  // iterate over all keys in the current object.
  for (const key in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (!obj.hasOwnProperty(key)) continue

    // if the key is 'layer' and the value is an array, process it.
    if (key === 'layer' && Array.isArray(obj[key])) {
      // remove layer elements with mark.hideInDownload
      obj[key] = obj[key].filter((layer) => !layer?.mark?.hideInDownload)
    } else if (typeof obj[key] === 'object') {
      // recursively process nested objects.
      removeNotForDownloadLayers(obj[key])
    }
  }
  return obj
}

export const downloadImage = async ({
  component,
  parsedSpec,
  state,
  title,
  subtitle,
  caption,
  source,
  imageUrl,
  slug,
}) => {
  let downloading = true

  // toggle download button icon
  const downloadButton = component.querySelector('.downloadImageButton')
  downloadButton.classList.add('downloading')

  // add temporarily css to prevent the download image to be visible if it's larger than the component
  component.style.overflow = 'hidden'

  // download image and remove temporarily element
  // this funcion is called after the visualisation is rendered
  const saveImage = async () => {
    const dataUrl = await toPng(element)
    saveAs(dataUrl, `${slug}.jpg`)
    element.remove()
    downloading = false
    // remove temporarily class and css
    downloadButton.classList.remove('downloading')
    component.style.overflow = 'unset'
  }

  // add temporarily element with fixed width
  const element = document.createElement('div')
  element.classList.add('download-image')
  component.append(element)

  if (component.type === 'image') {
    // fetch image and translate it to dataUrl to get around cors issues
    const dataUrl = await toDataURL(imageUrl)

    // render html for static image
    element.innerHTML = `
      <article>
        <hgroup>
            ${title ? `<h2 data-style-as='h4'>${title}</h2>` : ''}
            ${subtitle ? `<p data-small>${subtitle}</p>` : ''}
        </hgroup>
        <figure>
          <img src=${dataUrl} ></img>
          ${caption ? `<figcaption>${caption}</figcaption>` : ''}
        </figure>
        <footer>
          <div>
            <p data-small>${source ? `Bron: ${source}` : ''}</p>
            <p data-small>onderzoek.amsterdam.nl</p>
          </div>
        </footer>
      </article>
      `

    element.querySelector('img').onload = saveImage
  }

  if (component.type === 'vega') {
    // render html for vega visualisation
    element.innerHTML = `
    <article>
        <hgroup>
            ${title ? `<h2 data-style-as='h4'>${title}</h2>` : ''}
            ${subtitle ? `<p data-small>${subtitle}</p>` : ''}
        </hgroup>
        <figure>
          <div class="container"></div>
          ${caption ? `<figcaption>${caption}</figcaption>` : ''}
        </figure>
        <div class="params"></div>
        <footer>
          <div>
             <p data-small>${source ? `Bron: ${source}` : ''}</p>
            <p data-small>onderzoek.amsterdam.nl</p>
          </div>
        </footer>
    </article>`

    // render vega visualisation
    const { vegaEmbed } = window.osVisualisation.osVis
    const vis = element.querySelector('.container')
    const options = {
      config,
      renderer: 'svg',
      actions: false,
    }
    // create a copy of the spec and remove layers that are marked to be hidden in download
    const downloadSpec = removeNotForDownloadLayers(
      JSON.parse(JSON.stringify(parsedSpec)),
    )
    const staticView = await vegaEmbed(vis, downloadSpec, options)

    // after vega visualisation has rendered, inject the state of the responsive chart,
    // but keep the new width. then update the view
    const { view } = staticView
    const { width } = view.getState().signals
    view.setState({
      ...state,
      signals: { ...state?.signals, width },
    })
    await view.runAsync()

    // add param values
    renderParamList(downloadSpec, state.signals, element)

    saveImage()
  }

  if (component.type === 'map') {
    // render html for os map
    element.innerHTML = `
    <article>
      <hgroup>
          ${title ? `<h2 data-style-as='h4'>${title}</h2>` : ''}
          ${subtitle ? `<p data-small>${subtitle}</p>` : ''}
      </hgroup>
      <figure>
        <div class="deck">
          <canvas></canvas>
          <div class="legends"></div>
        </div>
        ${caption ? `<figcaption>${caption}</figcaption>` : ''}
      </figure>
      <div class="params"></div>
    <footer>
      <div>
        <p data-small>${source ? `Bron: ${source}` : ''}</p>
        <p data-small>onderzoek.amsterdam.nl</p>
      </div>
    </footer>
    </article>
    `

    // init and render deck.gl instance
    const { Deck, getLegends, updateDeckLayers } = window.osVisualisation.osMap
    const { initialViewState, layerData, params } = state
    // remove layers that are marked to be hidden in download
    const downloadLayers = layerData.filter(({ mark }) => !mark.hideInDownload)
    const layers = updateDeckLayers(downloadLayers, params)
    const canvas = element.querySelector('canvas')
    const height = parsedSpec?.height || 500
    element.querySelector('.deck').style.minHeight = `${height}px`

    new Deck({
      canvas,
      initialViewState,
      layers,
      // deviceProps: { preserveDrawingBuffer: true },
      onAfterRender: () => {
        const allLayersLoaded = layers.reduce(
          (acc, { isLoaded }) => acc && isLoaded !== false,
          true,
        )
        if (allLayersLoaded && downloading) saveImage()
      },
    })

    // add legend(s)
    element.querySelector('.legends').innerHTML = getLegends(layerData)

    // add param values
    renderParamList(parsedSpec, params, element)
  }
}
