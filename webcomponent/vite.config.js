import { defineConfig } from 'vite'

export default defineConfig({
  base: './',
  publicDir: '../public',
  build: {
    rollupOptions: {
      input: {
        app: './os-vis-component.html',
        'os-visualisation': './src/os-visualisation.js',
        'os-vega': './src/os-vega.js',
        'os-map': './src/os-maps.js',
        'os-download': './src/download.js',
        'os-visualisation-style': './src/os-visualisation-style.css',
      },
      output: {
        // don't hash output
        entryFileNames: `[name].js`,
        // chunkFileNames: `[name].js`,
        assetFileNames: `[name].[ext]`,
      },
    },
  },
  server: {
    open: '/os-vis-component.html',
  },
})
